﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace Liliveri
{
    public partial class AdminForum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] != null)
            {
                username_label.Text = Session["User_ID"].ToString();

            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }


        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString);
            try
            {
                con.Open();
                string query = "select count(*) from ForumTitle where ForumTitle ='" + TextBox4.Text + "'";
                SqlCommand cmd = new SqlCommand(query, con);
                int check = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if (check > 0)
                {
                    Response.Write("The Forum Title already exist.");
                }
                else
                {
                    string query1 = "insert into ForumTitle (ForumTitle, CreatedBy) values (@ForumTitle,@CreatedBy) ";
                    SqlCommand cmd1 = new SqlCommand(query1, con);
                    cmd1.Parameters.AddWithValue("@ForumTitle", TextBox4.Text);
                    cmd1.Parameters.AddWithValue("@CreatedBy", (Session["User_ID"].ToString()));
                    cmd1.ExecuteNonQuery();
                    Response.Redirect("AdminForum.aspx");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.ToString());
            }
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "OpenForum")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[rowIndex - 1];
                string ForumTitle = row.Cells[0].Text;
                string ForumCreated = row.Cells[1].Text;
                Session["ForumTitle"] = ForumTitle;
                Session["ForumCreated"] = ForumCreated;
                Response.Redirect("AdminForumContent.aspx");

            }
        }

    }
}