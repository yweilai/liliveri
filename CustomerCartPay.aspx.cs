﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace Liliveri
{
    public partial class CustomerCartPay : System.Web.UI.Page
    {
        int PayablePrice = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] != null)
            {
                username_label.Text = Session["User_ID"].ToString();
                CartTrackingNo.Text = Session["CartTrackingNo"].ToString();
                CartWeight.Text = Session["CartWeight"].ToString();
                CartDeparture.Text = Session["CartDeparture"].ToString();
                ItemName.Text = Session["CartItemName"].ToString();
                
                int Weight = Convert.ToInt32(Session["CartWeight"]);

                PayablePrice = Weight * 5;
                TotalAmount.Text = PayablePrice.ToString();

                string payment_method = DropDownList1.SelectedValue;


            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void btn_Pay(object sender, EventArgs e)
        {
            string cs = ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            string query = "DELETE FROM [Parcel] WHERE Id ='" + Convert.ToInt32(Session["CartId"]) + "'";
            SqlCommand cmd1 = new SqlCommand(query, con);
            cmd1.ExecuteNonQuery();

            string query2 = "insert into PaidParcel(TrackingNumber, ItemName, Departure, Arrival, Weight, Time, Recorder,CreatorID,WriteTime,DelFlag, PaidTime, PaidMethod, PaidAmount) values (@TrackingNumber, @ItemName, @Departure, @Arrival, @Weight, @Time, @Recorder,@CreatorID,@WriteTime,@DelFlag, @PaidTime, @PaidMethod, @PaidAmount) ";
            SqlCommand cmd2 = new SqlCommand(query2, con);
            cmd2.Parameters.AddWithValue("@TrackingNumber", Convert.ToInt32(Session["CartTrackingNo"]));
            cmd2.Parameters.AddWithValue("@ItemName", Session["CartItemName"].ToString());
            cmd2.Parameters.AddWithValue("@Departure", Session["CartDeparture"].ToString());
            cmd2.Parameters.AddWithValue("@Arrival", Session["CartArrival"].ToString());
            cmd2.Parameters.AddWithValue("@Weight", Convert.ToInt32(Session["CartWeight"]));
            cmd2.Parameters.AddWithValue("@Time", Convert.ToDateTime(Session["CartTime"].ToString()));
            cmd2.Parameters.AddWithValue("@Recorder", Session["CartRecorder"].ToString());
            cmd2.Parameters.AddWithValue("@CreatorID", Session["CartCretorId"].ToString());
            cmd2.Parameters.AddWithValue("@WriteTime", Convert.ToDateTime(Session["CartWriteTme"].ToString()));
            cmd2.Parameters.AddWithValue("@DelFlag", 0);
            cmd2.Parameters.AddWithValue("@PaidTime", DateTime.Now);
            cmd2.Parameters.AddWithValue("@PaidMethod", DropDownList1.SelectedValue);
            cmd2.Parameters.AddWithValue("@PaidAmount", PayablePrice);
            cmd2.ExecuteNonQuery();


            lbmsg.Text = "Payment has successfully done! You will be directed in a few seconds";
            string url = "CustomerPaymentHistory.aspx";
            StringBuilder js = new StringBuilder("<script language=\"javascript\">")
                    .Append("var ts = 3; setInterval(\"redirect()\",1000);")
                    .Append("function redirect(){  if(ts == 0){")
                    .Append("window.location.href=\"" + url + "\"; }else{")
                    .Append("document.body.innerHTML = \"msg \" + (ts--)+\"seconds\";}}")
                    .Append("</script>");
            Response.Write(js.ToString());

        }
    }
}