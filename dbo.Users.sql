﻿CREATE TABLE [dbo].[Users] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Username] VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    [Address]  VARCHAR (50) NULL,
    [Gender]   VARCHAR (50) NULL,
    [Position] VARCHAR (50) NULL,
    [Email]    VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
