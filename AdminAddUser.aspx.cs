﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;


namespace Liliveri
{
    public partial class AdminAddUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] != null)
            {
                username_label.Text = Session["User_ID"].ToString();

            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        protected void btnBack(object sender, EventArgs e)
        {
            Response.Redirect("AdminManageUser.aspx");
        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString);
            try
            {
                con.Open();
                string query = "select count(*) from users where Username ='" + TextBox4.Text + "'";
                SqlCommand cmd = new SqlCommand(query, con);
                int check = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if (check > 0)
                {
                    Response.Write("Username already exist.");
                }
                else
                {
                    string query1 = "insert into Users (Username, Email, Password, Address, Gender, Position, ContactNo) values (@username,@email,@password,@address,@gender,@Position, @ContactNo) ";
                    SqlCommand cmd1 = new SqlCommand(query1, con);
                    cmd1.Parameters.AddWithValue("@username", TextBox4.Text);
                    cmd1.Parameters.AddWithValue("@email", TextBox1.Text);
                    cmd1.Parameters.AddWithValue("@password", TextBox2.Text);
                    cmd1.Parameters.AddWithValue("@address", TextBox3.Text);
                    cmd1.Parameters.AddWithValue("@gender", RadioButtonList1.SelectedValue);

                    cmd1.Parameters.AddWithValue("@Position", RadioButtonList2.SelectedValue);
                    cmd1.Parameters.AddWithValue("@ContactNo", TextBox6.Text);
                    cmd1.ExecuteNonQuery();
                    Response.Redirect("Default.aspx");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.ToString());
            }
        }
    }
}
