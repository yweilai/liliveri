﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Liliveri.Default" %>



<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Liliveri- Sending Parcel from home</title>
<link rel="shortcut icon" href="img/favicon.ico">

<!-- =========================
     STYLESHEETS   
============================== -->

<!-- STYLES FILE -->   
<link href="css/master.css" rel="stylesheet">	
        
</head>
<body>

	<!-- =========================
     PRE LOADER       
	============================== -->
	
    <div class="preloader" id="preloader">
        <div class="cssload-container">
        	<div class="cssload-whirlpool"></div>
        </div>
    </div>
	
	<!-- =========================
     END PRE LOADER       
	============================== -->
	
	<!-- =========================
		TOP MAIN NAVBAR
	============================== -->
	<div class="main-navbar main-navbar-1" id="main-navbar">
		<div class="container">
			<div class="row">
                 
				<!-- === TOP LOGO === -->
				 
				<div class="logo" id="main-logo">
					<div class="logo-image">
						<img src="img/logo.png" alt="" />
					</div>
					<div class="logo-text">
						Liliveri
					</div>
				</div>
				 
				<!-- === TOP SEARCH === -->
				 
				<div class="main-search-input" id="main-search-input">
					<form>
						<input type="text" id="main-search" name="main-search" placeholder="Try and type enter..." />
					</form>
				</div>
				 

				
				<div class="show-menu-control">
					<!-- === top search button show === -->
					<a id="show-menu" class="show-menu" href="#">
						<div class="my-btn my-btn-primary">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-bars"></i>
                            </div>
						</div>
					</a>
				</div>
				 
				<!-- === TOP MENU === -->
								 
				<div class="collapse navbar-collapse main-menu main-menu-1" id="main-menu">
					<ul class="nav navbar-nav">
											
						<!-- === top menu item === -->
						<li class="active dropdown">
							<a href="Default.aspx">Home</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a  href="Service_Detail.aspx">Service</a>

						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li>
							<a href="About.aspx">About us</a>
						</li> 
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">Delivering Helper</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="Fare_Calculator.aspx">Fare Calculator</a>
								</li>
								<li>
									<a href="Tracking.aspx">Tracking No.</a>
								</li>
								
							</ul>
						</li> 
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li >
							<a class="latest" href="Contacts.aspx">Contacts</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">Client Area</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="Login.aspx">Login</a>
								</li>
								<li>
									<a href="Register.aspx">Create Free Account</a>
								</li>
							</ul>
						</li> 
	
					</ul>
				</div>

			</div>
		</div>
	</div>
	 
	 <!-- =========================
		END TOP MAIN NAVBAR
	============================== -->
     
	 
	 <!-- =========================
		MAIN SLIDER
	============================== -->
	<div id="main-slider-1" class="main-slider main-slider-1">
		<div class="sp-slides">
			
			<!-- === SLIDER ITEM === -->	
			
			<div class="sp-slide">
				
				<!-- === slide overlay === -->	
				<div class="main-slider-overlay"></div>
				
				<!-- === slide image === -->	
                <img class="sp-image" src="assets/slider-pro/css/images/blank.gif"
				data-src="media/main-slider/1.jpg"
				data-retina="media/main-slider/1.jpg" alt="" />
				
				<!-- === slide container === -->	
				<div class="container">
                    <div class="main-slider-content">
        				<h2 class="sp-layer"
                            data-position="rightCenter" data-horizontal="15" data-vertical="-92"
        					data-show-transition="left" data-hide-transition="left" data-show-delay="200" data-hide-delay="200">
        					Sending your parcel from 
        				</h2>
        
        				<h2 class="sp-layer color-primary"
                            data-position="rightCenter" data-horizontal="15" data-vertical="-40"
        					data-show-transition="left" data-hide-transition="left" data-show-delay="400" data-hide-delay="400">
        					We help you send to the courier station
        				</h2>
            
                        <a class="sp-layer main-button" href="#"
                            data-position="rightCenter" data-horizontal="15" data-vertical="55"
            				data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="600">
							<div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									TRY IT NOW
								</div>
							</div>
						</a>
                        
						<!-- === slide buttons === -->	
                        <div class="sp-layer main-slider-buttons" 
							data-position="rightCenter" data-horizontal="15" data-vertical="165" data-hide-delay="600">
							<a class="main-slider-button-prev" href="#">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										<i class="fa fa-angle-left"></i>
									</div>
								</div>
							</a>
							<a class="main-slider-button-next" href="#">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										<i class="fa fa-angle-right"></i>
									</div>
								</div>
							</a>
						</div>
                         
                    </div>
                </div>
			</div>

            <!-- === SLIDER ITEM === -->	
			
			<div class="sp-slide">
				
				<!-- === slide overlay === -->	
				<div class="main-slider-overlay"></div>
				
				<!-- === slide image === -->	
                <img class="sp-image" src="assets/slider-pro/css/images/blank.gif"
				data-src="media/main-slider/2.jpg"
				data-retina="media/main-slider/2.jpg" alt="" />
				
				<!-- === slide container === -->	
				<div class="container">
                    <div class="main-slider-content">
        				<h2 class="sp-layer"
                            data-position="rightCenter" data-horizontal="15" data-vertical="-92"
        					data-show-transition="down" data-hide-transition="down" data-show-delay="600" data-hide-delay="600">
        					No need to long queue in courier station
        				</h2>
        
        				<h2 class="sp-layer color-primary"
                            data-position="rightCenter" data-horizontal="15" data-vertical="-40"
        					data-show-transition="down" data-hide-transition="down" data-show-delay="400" data-hide-delay="400">
        					Let us do it
        				</h2>
            
                        <a class="sp-layer main-button" href="#"
                            data-position="rightCenter" data-horizontal="15" data-vertical="55"
            				data-show-transition="down" data-hide-transition="down" data-show-delay="200" data-hide-delay="200">
							<div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									TRY IT NOW
								</div>
							</div>
						</a>
                        
						<!-- === slide buttons === -->	
                        <div class="sp-layer main-slider-1-buttons" 
							data-position="rightCenter" data-horizontal="15" data-vertical="165" data-hide-delay="600">
							<a class="main-slider-button-prev" href="#">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										<i class="fa fa-angle-left"></i>
									</div>
								</div>
							</a>
							<a class="main-slider-button-next" href="#">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										<i class="fa fa-angle-right"></i>
									</div>
								</div>
							</a>
						</div>
                         
                    </div>
                </div>
			</div>
   
            <!-- === SLIDER ITEM === -->	
			
			<div class="sp-slide">
				
				<!-- === slide overlay === -->	
				<div class="main-slider-overlay"></div>
				
				<!-- === slide image === -->	
                <img class="sp-image" src="assets/slider-pro/css/images/blank.gif"
				data-src="media/main-slider/3.jpg"
				data-retina="media/main-slider/3.jpg" alt="" />
				
				<!-- === slide container === -->	
				<div class="container">
                    <div class="main-slider-content">
        				<h2 class="sp-layer"
                            data-position="rightCenter" data-horizontal="15" data-vertical="-92"
        					data-show-transition="up" data-hide-transition="up" data-show-delay="200" data-hide-delay="200">
        					Stay indoor and do your business online
        				</h2>
        
        				<h2 class="sp-layer color-primary"
                            data-position="rightCenter" data-horizontal="15" data-vertical="-40"
        					data-show-transition="up" data-hide-transition="up" data-show-delay="400" data-hide-delay="400">
        					Call us anytime to send your parcel to courier station
        				</h2>
            
                        <a class="sp-layer main-button" href="#"
                            data-position="rightCenter" data-horizontal="15" data-vertical="55"
            				data-show-transition="up" data-hide-transition="up" data-show-delay="600" data-hide-delay="600">
							<div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									TRY IT NOW
								</div>
							</div>
						</a>
                        
						<!-- === slide buttons === -->	
                        <div class="sp-layer main-slider-buttons" 
							data-position="rightCenter" data-horizontal="15" data-vertical="165" data-hide-delay="600">
							<a class="main-slider-button-prev" href="#">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										<i class="fa fa-angle-left"></i>
									</div>
								</div>
							</a>
							<a class="main-slider-button-next" href="#">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										<i class="fa fa-angle-right"></i>
									</div>
								</div>
							</a>
						</div>
                         
                    </div>
                </div>
			</div>

		</div>
    </div>
	<!-- =========================
		END MAIN SLIDER
	============================== -->
    
	
	<!-- =========================
		SERVICES
	============================== -->
    <section class="def-section home-services">
		<div class="container">
			<div class="row">
                 
				<!-- === SERVICE ITEM === -->	
				
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				
					<div class="service">				
						<div class="service-icon">
							<i class="flaticon-transport222"></i>
						</div>
						<h3>Choose Any one Logistic that covered</h3>
						<div class="service-text">
							<p>Break the rules of monopoly! We give the right of choice</p>
						</div>
						<div class="service-button">
							<a href="Service_Detail.aspx">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										MORE
									</div>
								</div>
							</a>
						</div>
					</div>
					
				</div>
                
				<!-- === SERVICE ITEM === -->
				
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="service-mark">
				
					<div class="service-mark-border-top"></div>
					<div class="service-mark-border-right"></div>
					<div class="service-mark-border-bottom"></div>
					<div class="service-mark-border-left"></div>
					<div class="service">
						<div class="service-icon">
							<i class="flaticon-transport358"></i>
						</div>
						<h3>24/7 Anytime Anywhere</h3>
						<div class="service-text">
							<p>No need to wait operating hours, because we operate 24 hours 7 days a week.</p>
						</div>
						<div class="service-button">
							<a href="Service_Detail.aspx">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										MORE
									</div>
								</div>
							</a>
						</div>
					</div>
					
				</div>
                
				<!-- === SERVICE ITEM === -->
				
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				
					<div class="service">
						<div class="service-icon">
							<i class="flaticon-transport548"></i>
						</div>
						<h3>Various kind of Land Transport</h3>
						<div class="service-text">
							<p>We do have lorry, van and private car. So we can carry large and small product.</p>
						</div>
                        <div class="service-button">
							<a href="Service_Detail.aspx">
								<div class="my-btn my-btn-default">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										MORE
									</div>
								</div>
							</a>
						</div>
                     </div>
					 
                 </div>
				 
             </div>
         </div>
     </section>
	 <!-- =========================
		END SERVICES
	============================== -->
    
	
	<!-- ===================================
		SECTION ABOUT US AND GET QUOTE
	======================================== -->
	<section class="def-section about-quote">
		<div class="section-bg-left"></div>
		<div class="section-bg-right"></div>
		<div class="container">
			<div class="row">
				
				<!-- === ABOUT US === -->	
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 home-about">
						
						<!-- === TITLE GROUP === -->	
						<div class="title-group">
							<h2>ABOUT US</h2>
							<div class="subtitle with-square">A new style of logistic assistance</div>
						</div>
						
						<!-- === ABOUT US TEXT === -->	
						<p>
							We assist the public to get access logistic service easily. Since 2020, we started operates.
						</p>
						

							
							<!-- === READ MORE BUTTON === -->	
							<a href="About.aspx"><div class="home-about-button">
								<div class="my-btn my-btn-primary">
									<div class="my-btn-bg-top"></div>
									<div class="my-btn-bg-bottom"></div>
									<div class="my-btn-text">
										READ
									</div>
								</div>
							</div></a>

						</div>
				</div>
				
				

			</div>
		</div>
	</section>
	 <!-- ===================================
		END SECTION ABOUT US AND GET QUOTE
	======================================== -->
     
	 <!-- ===================================
	    SECTION REVIEWS AND FAQ
	======================================== -->
	
				
				<!-- === FAQ === -->
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="home-faq">
						
						<!-- === TITLE GROUP === -->

						
						<!-- === ACCORDION === -->
						<div class="panel-group" id="accordion">
							


							

								<div id="collapseThree" class="panel-collapse collapse">
									<div class="panel-body">
										<p>In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. 
										Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra
										rhoncus pede.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</section>
	 <!-- ===================================
		END SECTION REVIEWS AND FAQ
	======================================== -->
	
	
	<!-- ===================================
		SECTION STATS
	======================================== -->
	<div class="def-section stats" id="home-stats" data-stellar-background-ratio="0.4">
		<div class="stats-overlay"></div>
		<div class="container">
			<div class="row">
				
				<!-- === STATS ITEM === -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="stat-item">
						<div class="stat-item-icon">
							<i class="flaticon-first33"></i>
						</div>
						<div class="stat-item-number" id="num1">
							50000
						</div>
						<div class="stat-item-text">
							PARCEL<br>SENT
						</div>
					</div>
				</div>
				
				<!-- === STATS ITEM === -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="stat-item stat-item-mark">
						<div class="stat-item-icon">
							<i class="flaticon-shirt16"></i>
						</div>
						<div class="stat-item-number" id="num2">
							1000
						</div>
						<div class="stat-item-text">
							CLIENTS<br>SASTIFIED
						</div>
					</div>
				</div>
				
				<!-- === STATS ITEM === -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="stat-item">
						<div class="stat-item-icon">
							<i class="flaticon-group2"></i>
						</div>
						<div class="stat-item-number" id="num3">
							200
						</div>
						<div class="stat-item-text">
							PARCEL<br>RUNNERS
						</div>
					</div>
				</div>
				
				<!-- === STATS ITEM === -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="stat-item">
						<div class="stat-item-icon">
							<i class="flaticon-clock96"></i>
						</div>
						<div class="stat-item-number" id="num4">
							2
						</div>
						<div class="stat-item-text">
							YEARS IN<br>MARKET
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- ===================================
		END SECTION STATS
	======================================== -->
    
	

	<!-- ===================================
		CLIENTS SECTION
	======================================== -->
	<section class="def-section" id="clients-section">
		<div class="home-big-image" data-stellar-background-ratio="0.4"></div>
		<div class="container">
			<div class="row">
			
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="home-clients">
						
						<!-- === TITLE GROUP === -->
						<div class="title-group">
							<h2>CLIENTS</h2>
							<div class="subtitle with-square">SASTIFIED CLIENTS</div>
						</div>
						<p>
						We serves everyone. No matter your background, regardless who you are, we treat all clients equally.
						</p>
						
						<!-- === OWL CAROUSEL === -->
						<div class="home-clients-carousel owl-carousel owl-theme" id="owl-clients">
							
							<!-- === OWL CAROUSEL ITEM === -->
							<div class="home-clients-carousel-block">
								
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client1.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item ">
									<a href="#"><img src="media/clients/client2.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client3.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client4.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client5.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client6.png" alt="" /></a>
								</div>
								<div class="home-clients-carousel-hline"></div>
								<div class="home-clients-carousel-vline1"></div>
								<div class="home-clients-carousel-vline2"></div>
							</div>
							
							<!-- === OWL CAROUSEL ITEM === -->
							<div class="home-clients-carousel-block">
								
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client1.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item ">
									<a href="#"><img src="media/clients/client2.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client3.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client4.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client5.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client6.png" alt="" /></a>
								</div>
								<div class="home-clients-carousel-hline"></div>
								<div class="home-clients-carousel-vline1"></div>
								<div class="home-clients-carousel-vline2"></div>
							</div>
							
							<!-- === OWL CAROUSEL ITEM === -->
							<div class="home-clients-carousel-block">
								
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client1.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item ">
									<a href="#"><img src="media/clients/client2.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client3.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client4.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client5.png" alt="" /></a>
								</div>
								<!-- === client item === -->
								<div class="home-clients-carousel-item">
									<a href="#"><img src="media/clients/client6.png" alt="" /></a>
								</div>
								<div class="home-clients-carousel-hline"></div>
								<div class="home-clients-carousel-vline1"></div>
								<div class="home-clients-carousel-vline2"></div>
							</div>
						</div>
						
						<!-- === OWL CAROUSEL BUTTONS === -->
						<div class="home-clients-buttons">
							<a id="prev-clients" href="#"><div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									<i class="fa fa-angle-left"></i>
								</div>
							</div></a>
							<a id="next-clients" href="#"><div class="my-btn my-btn-grey">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									<i class="fa fa-angle-right"></i>
								</div>
							</div></a>
						</div>
						
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- ===================================
		END CLIENTS SECTION
	======================================== -->
	 
	<!-- ===================================
		SUBSCRIBE SECTION
	======================================== -->

	<!-- ===================================
		END SUBSCRIBE SECTION
	======================================== -->
     
	
	<!-- ===================================
		FOOTER
	======================================== -->
	<footer class="def-section footer">
		<div class="container">
			<div class="row">
				
				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
					<div class="logo with-border-bottom">
						<div class="logo-image">
							<img src="img/logo.png" alt="" />
						</div>
						<div class="logo-text">
							Liliveri
						</div>
					</div>
					<div class="footer-1-text">
						<p>We recommended Google Chrome and Microsoft Edge. </p>
					</div>
					<div class="footer-1-button">
						<a href="About.aspx"><div class="my-btn my-btn-primary">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								MORE
							</div>
						</div></a>
					</div>
				</div>
				
				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
					<h4 class="with-square with-border-bottom">LINKS</h4>
					<div class="footer-2-links">
						<div class="footer-2-links-1">
							<ul>
								<li><a href="Default.aspx">Home</a></li>
								<li><a href="Service_Detail.aspx">Service</a></li>
								<li><a href="About.aspx">About us</a></li>
								<li><a href="Login.aspx">Clients</a></li>
							</ul>
						</div>
						<div class="footer-2-links-2">
							<ul>
								<li><a href="Contacts.aspx">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
					<h4 class="with-square with-border-bottom">ADRESS</h4>
					<div class="footer-3-phone footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-phone"></i></span>
						Telephone:  + 390 12 345 6789
					</div>
					<div class="footer-3-fax footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-fax"></i></span>
						Fax/phone:  + 390 12 345 6789
					</div>
					<div class="footer-3-mail footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
						E-mail:  customerrelationship@liliveri.com.my
					</div>
					<div class="footer-3-adress footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-map-marker"></i></span>
						Adress: Welman Street, Rawang Town, Rawang 48000, Selangor Dahrul Ehsan
					</div>
				</div>
				
			</div>
		</div>
	</footer>
	<!-- ===================================
		END FOOTER
	======================================== -->


	<!-- ===================================
		BOTTOM SECTION
	======================================== -->
	<div class="bottom">
		<div class="container">
			<div class="row">
				
				<!-- === BOTTOM LEFT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
					COPYRIGHT 2021 | Liliveri
				</div>
				
				<!-- === BOTTOM CENTER === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
					<a href="#"><div class="my-btn my-btn-grey">
						<div class="my-btn-bg-top"></div>
						<div class="my-btn-bg-bottom"></div>
						<div class="my-btn-text">
							<i class="fa fa-twitter"></i>
						</div>
					</div></a>
					<a href="#"><div class="my-btn my-btn-grey">
						<div class="my-btn-bg-top"></div>
						<div class="my-btn-bg-bottom"></div>
						<div class="my-btn-text">
							<i class="fa fa-facebook"></i>
						</div>
					</div></a>
					<a href="#"><div class="my-btn my-btn-grey">
						<div class="my-btn-bg-top"></div>
						<div class="my-btn-bg-bottom"></div>
						<div class="my-btn-text">
							<i class="fa fa-google-plus"></i>
						</div>
					</div></a>
					<a href="#"><div class="my-btn my-btn-grey">
						<div class="my-btn-bg-top"></div>
						<div class="my-btn-bg-bottom"></div>
						<div class="my-btn-text">
							<i class="fa fa-pinterest-p"></i>
						</div>
					</div></a>
					<a href="#"><div class="my-btn my-btn-grey">
						<div class="my-btn-bg-top"></div>
						<div class="my-btn-bg-bottom"></div>
						<div class="my-btn-text">
							<i class="fa fa-instagram"></i>
						</div>
					</div></a>
				</div>
				
				<!-- === BOTTOM RIGHT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">
					<a href="#">TERM OF USE</a> | 
					MADE BY <a href="#">Liliveri</a>
				</div>
				
			</div>
		</div>
	</div>
	<!-- ===================================
		END BOTTOM SECTION
	======================================== -->
	
	<!-- =========================
	   SLIDE MENU
	============================== -->
	<aside id="slide-menu" class="slide-menu">
		
		<!-- === CLOSE MENU BUTON === -->	
		<div class="close-menu" id="close-menu">
			<i class="fa fa-close"></i>
		</div>
		
		<!-- === SLIDE MENU === -->	
		<ul id="left-menu" class="left-menu">
			
			<!-- === SLIDE MENU ITEM === -->	
			<li> 
				<a href="Default.aspx">Home <i class="fa fa-plus arrow"></i></a>
				
			</li>
			
			<!-- === SLIDE MENU ITEM === -->	
			<li> 
				<a href="#">Service <i class="fa fa-plus arrow"></i></a>
				
				<!-- === slide menu child === -->	
				<ul class="slide-menu-child">
					<li><a href="Service_Detail.aspx">Service Details</a></li>
				</ul>
			</li>
			
			<!-- === SLIDE MENU ITEM === -->	
			<li> 
				<a href="About.aspx">About us</a>
			</li>
			
			<!-- === SLIDE MENU ITEM === -->	
			<li> 
				<a href="#">Delivering Helper <i class="fa fa-plus arrow"></i></a>
				
				<!-- === slide menu child === -->	
				<ul class="slide-menu-child">
					<li><a href="07_typography.html">Fare Calculator</a></li>
					<li><a href="Tracking.aspx">Tracking No.</a></li>
				</ul>
			</li>
			<li>
				<a href="Contacts.aspx">Contacts</a>
			</li>
			
			<!-- === SLIDE MENU ITEM === -->	
			<li>
				<a href="#">Client Area<i class="fa fa-plus arrow"></i></a>
				
				<!-- === slide menu child === -->	
				<ul class="slide-menu-child">
					<li><a href="Register.aspx">Create a free account</a></li>
					<li><a href="Login.aspx">Login</a></li>
				</ul>
			</li>
			
			<!-- === SLIDE MENU ITEM === -->	

		</ul>
		
	</aside>
	<!-- =========================
	   END SLIDE MENU
	============================== -->

	<!-- =========================
	   BLACK OVERLAY
	============================== -->
	<div class="black-overlay" id="black-overlay"></div>
	<!-- =========================
	   END BLACK OVERLAY
	============================== -->

	
	<!-- =========================
		 SCRIPTS   
	============================== -->	
	
	<!-- JQUERY -->
	<script src="js/jquery-1.11.3.min.js"></script>
	
	<!-- BOOTSTRAP -->
	<script src="js/bootstrap.min.js"></script>
	
	<!-- SMOOTH SCROLLING  -->
	<script src="js/smoothscroll.min.js"></script>
	
	<!-- STELLAR.JS FOR PARALLAX -->
	<script src="js/jquery.stellar.min.js"></script>
	
	<!-- SLIDER PRO  -->
	<script src="assets/slider-pro/js/jquery.sliderPro.min.js"></script>
	
	<!-- SCROLLSPY -->
	<script src="js/scrollspy.min.js"></script>
	
	<!-- WOW PLAGIN -->
	<script src="js/wow.min.js"></script>
	
	<!-- CAROUSEL -->
	<script src="assets/owl-carousel/owl.carousel.min.js"></script>
	
	<!-- VERTICAL ACCORDEON MENU -->
	<script src="js/metisMenu.min.js"></script>
	
	<!-- CUSTOM SCRIPT -->
	<script src="js/theme.min.js"></script>
	
</body>
</html>