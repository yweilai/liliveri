﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerParce.aspx.cs" Inherits="Liliveri.CustomerParce" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    
    

    <!-- SITE TITLE -->
    <title>Liliveri</title>


    <style type="text/css">
        html, body {
            height: 100%;
            overflow: auto;
        }

        .addparce {
            width: auto;
            height: 50px;
            float: right;
            color: #fff;
            font-size: 20px;
            font-weight: 700;
            background-color: #199ed8;
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px
        }

            .addparce:hover {
                background-color: #199ed8
            }
            .well {
        vertical-align: middle !important;
        margin-bottom: 0px;
        padding:30px !important;
    }
             .well .ul1 {
            float: left;
        }

        .well .ul2 {
            float: right;
        }

        .well ul {
            list-style: none;
        }

            .well ul li {
                height: 85px;
                float: left;
                padding-right: 45px;
                text-align: center;
                vertical-align: middle !important;
            }

                .well ul li button {
                    margin-top: -8px;
                }
    </style>
    <!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
    <link rel="shortcut icon" href="img/favicon.ico">

    <!-- =========================
     STYLESHEETS   
============================== -->

    <!-- STYLES FILE -->
    <link href="css/master.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>


    <!-- =========================
	 PRE LOADER
	============================== -->

    <div class="preloader" id="preloader">
        <div class="cssload-container">
            <div class="cssload-whirlpool"></div>
        </div>
    </div>

    <!-- =========================
	 END PRE LOADER
	============================== -->
    <!-- =========================
		TOP MAIN NAVBAR
	============================== -->
    <div class="main-navbar main-navbar-1" id="main-navbar">
        <div class="container">
            <div class="row">

                <!-- === TOP LOGO === -->

                <div class="logo" id="main-logo">
                    <div class="logo-image">
                        <img src="img/logo.png" alt="" />
                    </div>
                    <div class="logo-text">
                        Liliveri
                    </div>
                </div>





                <div class="show-menu-control">
                    <!-- === top search button show === -->
                    <a id="show-menu" class="show-menu" href="#">
                        <div class="my-btn my-btn-primary">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- === TOP MENU === -->

                <div class="collapse navbar-collapse main-menu main-menu-1" id="main-menu">
                    <ul class="nav navbar-nav">

                        <!-- === top menu item === -->
                        <li class="active dropdown">
                            <a href="CustomerHome.aspx">Home</a>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li class="dropdown">
                            <a href="CustomerParce.aspx">Parcel Send Request</a>

                        </li>
                        		<li class="main-menu-separator"></li>
									<li class="dropdown">
							<a  href="CustomerCart.aspx">Cart</a>

						</li>
						<li class="main-menu-separator"></li>
									<li class="dropdown">
							<a  href="CustomerPaymentHistory.aspx">Payment History</a>

						</li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li>
                            <a href="CustomerForum.aspx">Forum</a>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#">Delivering Helper</a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="CustomerFare_Calculator.aspx">Fare Calculator</a>
                                </li>
                                <li>
                                    <a href="CustomerTracking.aspx">Tracking No.</a>
                                </li>

                            </ul>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li>
                            <a class="latest" href="CustomerContacts.aspx">Contacts</a>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#">
                                <asp:Label ID="username_label" runat="server" Text="username_label"></asp:Label>
                                Information</a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="CustomerChangeDetails.aspx">Update Details</a>
                                </li>
                                <li>
                                    <a href="Logout.aspx">Logout</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!-- =========================
		END TOP MAIN NAVBAR
	============================== -->
    <!-- ===================================
		PAGE HEADER
	======================================== -->
    <div class="page-header" data-stellar-background-ratio="0.4">
        <div class="page-header-overlay"></div>
        <div class="container">
            <div class="row">

                <!-- === PAGE HEADER TITLE === -->
                <div class="page-header-title">
                    <h2>Parcel Send Request</h2>
                </div>

                <!-- === PAGE HEADER BREADCRUMB === -->
                <div class="page-header-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Parcel Send Request</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- ===================================
		END PAGE HEADER
	======================================== -->
    <!---- Login Part ---->
    <div id="App_CustomerParce">
        <input type="button" value="Add Parce" class="addparce" v-on:click="AddParce">
        <div class="container-fluid" style="text-align: center;">
            <div class="row">
                <div class="col-md-12" style="overflow: auto;">
                    <table style="width: 100%; border-collapse: collapse; table-layout: fixed; word-wrap: break-word" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 57px;">
                                    <input type="checkbox" name="chkAllToLabelGroupItem" style="vertical-align: middle;" v-on:click="AllChecked" />
                                    <span style="vertical-align: middle;">All</span>
                                </th>
                                <th style="width: 57px;">Number</th>
                                <th style="width: 120px;">Tracking No</th>
                                <th style="width: 100px;">Item Name</th>
                                <th style="width: 50px;">Departure</th>
                                <th style="width: 100px;">Arrival</th>
                                <th style="width: 100px;">Weight (Kg)</th>
                                <th style="width: 110px;">Recording Time</th>
                                <th style="width: 110px;">Recorder</th>
                                <th style="width: 140px;">Operation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item,index) in CustomerParceList">
                                <td>
                                    <input type="checkbox" name="chkGroupItem" style="vertical-align: middle;" v-on:click="singleChecked(item)" ref="chkGroupItem" v-bind:index="index" />
                                </td>
                                <td>{{item.Id}}</td>
                                <td>{{item.TrackingNumber}}</td>
                                <td>{{item.ItemName}}</td>
                                <td>{{item.Departure }}</td>
                                <td>{{item.Arrival }}</td>
                                <td>{{item.Weight }}</td>
                                <td>{{item.Time }}</td>
                                <td>{{item.Recorder}}</td>
                                <td>
                                    <%--<a title="编辑" v-on:click="GetCustomerParce(item)" style="cursor:pointer;">编辑</a>--%>  
                                    <a title="删除" v-on:click="Remove(item)" style="cursor: pointer;">Delete</a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10" v-if="Object.keys(CustomerParceList).length==0" style="text-align: center !important; font-weight: bold;">
                                    <div style="text-align: center;">
                                        <img src="Scripts/image/noData.png" style="width: 240px; height: 160px;" />
                                        <p style="margin-top: 24px; color: rgb(0, 0, 0); font-size: 20px;">No Data&nbsp;</p>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="well">
                <div class="dul">
                    <ul class="ul2" >
                        <li>Selected<strong style="color: red">&nbsp;{{productNum}}</strong> Pcs Parcel
                        </li>
                        <li>

                            	<form id="form1" runat="server">

                            <asp:Button CssClass="btn btn-default " ID="Button1" runat="server" OnClick="Button1_Click" Text="Cart" />
                                    </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal fade" id="AddParceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="GroupLabelTitle">Add Parcel</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Tracking No</label>
                                <input type="text" class="form-control" v-model="Parce.TrackingNumber">
                            </div>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Item Name</label>
                                <input type="text" class="form-control" v-model="Parce.ItemName">
                            </div>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Departure</label>
                                <input type="text" class="form-control" v-model="Parce.Departure">
                            </div>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Arrival</label>
                                <input type="text" class="form-control" v-model="Parce.Arrival">
                            </div>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Weight (Kg)</label>
                                <input type="text" class="form-control" v-model="Parce.Weight">
                            </div>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Recording Time</label>
                                <input type="date" class="form-control" v-model="Parce.Time">
                            </div>
                            <div class="form-group">
                                <label for="labelGroupName" class="control-label">Recorder</label>
                                <input type="text" class="form-control" v-model="Parce.Recorder">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" v-on:click="SaveCustomerParce">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 添加包裹 Modal -->

    <!-- ===================================
		FOOTER
	======================================== -->
    <footer class="def-section footer">
        <div class="container">
            <div class="row">

                <!-- === FOOTER COLUMN === -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
                    <div class="logo with-border-bottom">
                        <div class="logo-image">
                            <img src="img/logo.png" alt="" />
                        </div>
                        <div class="logo-text">
                            Liliveri
                        </div>
                    </div>
                    <div class="footer-1-text">
                        <p>We recommended Google Chrome and Microsoft Edge. </p>
                    </div>
                    <div class="footer-1-button">
                        <a href="About.aspx">
                            <div class="my-btn my-btn-primary">
                                <div class="my-btn-bg-top"></div>
                                <div class="my-btn-bg-bottom"></div>
                                <div class="my-btn-text">
                                    MORE
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <!-- === FOOTER COLUMN === -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
                    <h4 class="with-square with-border-bottom">LINKS</h4>
                    <div class="footer-2-links">
                        <div class="footer-2-links-1">
                            <ul>
                                <li><a href="Default.aspx">Home</a></li>
                                <li><a href="Service_Detail.aspx">Service</a></li>
                                <li><a href="About.aspx">About us</a></li>
                                <li><a href="Login.aspx">Clients</a></li>
                            </ul>
                        </div>
                        <div class="footer-2-links-2">
                            <ul>
                                <li><a href="Contacts.aspx">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- === FOOTER COLUMN === -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
                    <h4 class="with-square with-border-bottom">ADRESS</h4>
                    <div class="footer-3-phone footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-phone"></i></span>
                        Telephone:  + 390 12 345 6789
                    </div>
                    <div class="footer-3-fax footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-fax"></i></span>
                        Fax/phone:  + 390 12 345 6789
                    </div>
                    <div class="footer-3-mail footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
                        E-mail:  customerrelationship@liliveri.com.my
                    </div>
                    <div class="footer-3-adress footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-map-marker"></i></span>
                        Adress: Welman Street, Rawang Town, Rawang 48000, Selangor Dahrul Ehsan
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <!-- ===================================
		END FOOTER
	======================================== -->
    <!-- ===================================
		BOTTOM SECTION
	======================================== -->
    <div class="bottom">
        <div class="container">
            <div class="row">

                <!-- === BOTTOM LEFT === -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
                    COPYRIGHT 2021 | Liliveri
                </div>

                <!-- === BOTTOM CENTER === -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-twitter"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-google-plus"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-pinterest-p"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-instagram"></i>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- === BOTTOM RIGHT === -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">
                    <a href="#">TERM OF USE</a> |
					MADE BY <a href="#">Liliveri</a>
                </div>

            </div>
        </div>
    </div>
    <!-- ===================================
		END BOTTOM SECTION
	======================================== -->
    <!-- =========================
	   SLIDE MENU
	============================== -->
    <aside id="slide-menu" class="slide-menu">

        <!-- === CLOSE MENU BUTON === -->
        <div class="close-menu" id="close-menu">
            <i class="fa fa-close"></i>
        </div>

        <!-- === SLIDE MENU === -->
        <ul id="left-menu" class="left-menu">

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="Default.aspx">Home <i class="fa fa-plus arrow"></i></a>

            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="#">Service <i class="fa fa-plus arrow"></i></a>

                <!-- === slide menu child === -->
                <ul class="slide-menu-child">
                    <li><a href="Service_Detail.aspx">Service Details</a></li>
                </ul>
            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="About.aspx">About us</a>
            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="#">Delivering Helper <i class="fa fa-plus arrow"></i></a>

                <!-- === slide menu child === -->
                <ul class="slide-menu-child">
                    <li><a href="07_typography.html">Fare Calculator</a></li>
                    <li><a href="Tracking.aspx">Tracking No.</a></li>
                </ul>
            </li>
            <li>
                <a href="Contacts.aspx">Contacts</a>
            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="#">Client Area<i class="fa fa-plus arrow"></i></a>

                <!-- === slide menu child === -->
                <ul class="slide-menu-child">
                    <li><a href="Register.aspx">Create a free account</a></li>
                    <li><a href="Login.aspx">Login</a></li>
                </ul>
            </li>

            <!-- === SLIDE MENU ITEM === -->

        </ul>

    </aside>
    <!-- =========================
	   END SLIDE MENU
	============================== -->
    <!-- =========================
	   BLACK OVERLAY
	============================== -->
    <div class="black-overlay" id="black-overlay"></div>
    <!-- =========================
	   END BLACK OVERLAY
	============================== -->
    <!-- =========================
		 SCRIPTS
	============================== -->
    <!-- JQUERY -->
    <script src="js/jquery-1.11.3.min.js"></script>

    <!-- BOOTSTRAP -->
    <script src="js/bootstrap.min.js"></script>

    <!-- SMOOTH SCROLLING  -->
    <script src="js/smoothscroll.min.js"></script>

    <!-- STELLAR.JS FOR PARALLAX -->
    <script src="js/jquery.stellar.min.js"></script>

    <!-- SLIDER PRO  -->
    <script src="assets/slider-pro/js/jquery.sliderPro.min.js"></script>

    <!-- SCROLLSPY -->
    <script src="js/scrollspy.min.js"></script>

    <!-- WOW PLAGIN -->
    <script src="js/wow.min.js"></script>

    <!-- CAROUSEL -->
    <script src="assets/owl-carousel/owl.carousel.min.js"></script>

    <!-- VERTICAL ACCORDEON MENU -->
    <script src="js/metisMenu.min.js"></script>

    <!-- CUSTOM SCRIPT -->
    <script src="js/theme.min.js"></script>
</body>
</html>

<script src="Scripts/vue.min.js"></script>
<script src="Scripts/layer/layer.js"></script>
<script type="text/javascript">
    var vm = new Vue({
        el: "#App_CustomerParce",
        data: {
            productNum: 0,//表格已选中checkbox的数量
            CustomerParceList: [],//数据容器对象
            Parce: {//双向绑定的数据对象，即新增的实体类
                TrackingNumber: "",
                ItemName: "",
                Departure: "",
                Arrival: "",
                Weight: "",
                Time: "",
                Recorder: ""
            }
        },
        mounted: function (event) {
            this.LoadCustomerParceList();//绑定数据
        },
        methods: {
            //查询数据列表
            LoadCustomerParceList: function () {
                var _self = this;
                $.ajax({
                    type: "get",
                    url: "Handler/CustomerParceHandler.ashx",
                    data: { requestMethod: "GetCustomerParceList" },
                    beforeSend: function () {
                        
                    },
                    success: function (data) {
                        _self.CustomerParceList = eval('(' + data + ')');
                    },
                    error: function (err) {
                        alert(err);
                    }
                });
            },
            //点击新增包裹按钮弹出框
            AddParce: function () {
                $("#AddParceModal").modal('show');
                var _self = this;
                _self.Parce.TrackingNumber = "";
                _self.Parce.ItemName = "";
                _self.Parce.Departure = "";
                _self.Parce.Arrival = "";
                _self.Parce.Weight = "";
                _self.Parce.Time = "";
                _self.Parce.Recorder = "";
            },
            //保存数据
            SaveCustomerParce: function () {
                var _self = this;
                if (_self.Parce.TrackingNumber == "") {
                    layer.msg("Please Enter Tracking No！");
                    return;
                }
                else if (_self.Parce.ItemName == "") {
                    layer.msg("Please Enter Item Name！");
                    return;
                }
                else if (_self.Parce.Departure == "") {
                    layer.msg("Please Enter Departure！");
                    return;
                }
                else if (_self.Parce.Arrival == "") {
                    layer.msg("Please Enter Arrival！");
                    return;
                }
                else if (_self.Parce.Weight == "") {
                    layer.msg("Please Enter Weight (Kg)！");
                    return;
                }
                else if (_self.Parce.Time == "") {
                    layer.msg("Please Enter Recording Time！");
                    return;
                }
                else {
                    $.ajax({
                        type: "get",
                        url: "Handler/CustomerParceHandler.ashx",
                        data: {
                            requestMethod: "AddCustomerParce",
                            TrackingNumber: _self.Parce.TrackingNumber,
                            ItemName: _self.Parce.ItemName,
                            Departure: _self.Parce.Departure,
                            Arrival: _self.Parce.Arrival,
                            Weight: _self.Parce.Weight,
                            Time: _self.Parce.Time,
                            Recorder: _self.Parce.Recorder
                        },
                        success: function (data) {
                            if (data == "Record Successful!") {
                                $("#AddParceModal").modal('hide');
                                _self.LoadCustomerParceList();
                            }
                            else {
                                layer.msg(data);
                            }
                        },
                        error: function (err) {
                            layer.msg(err);
                        }
                    });
                }
            },
            //删除数据
            Remove: function (item) {
                var _self = this;
                layer.confirm('Are you sure to delete 【' + item.ItemName + '】?', function (index) {
                    $.ajax({
                        type: "get",
                        url: "Handler/CustomerParceHandler.ashx",
                        data: {
                            requestMethod: "RemoveCustomerParce",
                            id: item.Id
                        },
                        success: function (data) {
                            if (data == "Delete Successfully") {
                                _self.LoadCustomerParceList();
                            }
                            else {
                                layer.msg(data);
                            }
                        }
                    });
                    layer.close(index);
                });
            },
            //单选
            singleChecked: function (item) {
                if (typeof item.checked == 'undefined') {
                    this.$set(item, "checked", true);
                }
                else {
                    item.checked = !item.checked;
                }
                if (item.checked) {
                    vm.productNum++;
                }
                else {
                    vm.productNum--;
                }
            },
            //全选
            AllChecked: function (event) {
                //选中状态为true
                if (event.currentTarget.checked) {
                    //遍历所有的 chkGroupItem 改变其选中状态
                    $.each(vm.$refs.chkGroupItem, function (i, item) {
                        if (item.checked !== true) {
                            item.checked = true;
                        }
                    });
                    vm.productNum = vm.CustomerParceList.length;
                }
                else {
                    $.each(vm.$refs.chkGroupItem, function (i, item) {
                        if (item.checked === true) {
                            item.checked = false;
                        }
                    });
                    vm.productNum = 0;
                }
            }
        }
    });
</script>