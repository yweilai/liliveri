﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liliveri.Models
{
    public class Entity_Parce
    {
        public int Id { get; set; }
        public string TrackingNumber { get; set; }
        public string ItemName { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public int Weight { get; set; }
        public string Time { get; set; }
        public string Recorder { get; set; }
        public string CreatorID { get; set; }
        public DateTime WriteTime { get; set; }
        public int DelFlag { get; set; }
    }
}