﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Liliveri
{
    public partial class AdminManageParcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_ID"] != null)
                {
                    username_label.Text = Session["User_ID"].ToString();
                    
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }

        void loadContent()
        {
            string cs = ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [PaidParcel] WHERE TrackingNumber ='" + Convert.ToInt32(txtSearch.Text) + "'";
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();

            da.Fill(dt);
            DataColumn newCol = new DataColumn("RowNumber", typeof(string));
            dt.Columns.Add(newCol);
            int totalrow = 0;
            foreach (DataRow row in dt.Rows)
            {
                totalrow++;
                row["RowNumber"] = totalrow.ToString();
            }
            dt2 = dt;
            GridView1.DataSource = dt2;
            GridView1.DataBind();
            con.Close();
        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {


        }


        protected void GridView1_DataBound(object sender, EventArgs e)
        {

        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ParcelUpdate")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[rowIndex - 1];
                string ParcelId = row.Cells[1].Text;
                string ParcelTrackingNo = row.Cells[2].Text;
              
                Session["ParcelId"] = ParcelId;
                Session["ParcelTrackingNo"] = ParcelTrackingNo;
               
                Response.Redirect("AdminManageParcelContent.aspx");

            }
        }
        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            DataBind();
            loadContent();
            GridView1.Visible = true;
        }
    }
}