﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Liliveri
{
    public partial class CustomerTracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] != null)
            {
                username_label.Text = Session["User_ID"].ToString();

            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            GridView1.Visible = true;
            GridView2.Visible = true;
        }
    }
}