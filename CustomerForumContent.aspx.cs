﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Liliveri
{
    public partial class CustomerForumContent : System.Web.UI.Page
    {
        protected string ForumTitle;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] != null)
            {
                username_label.Text = Session["User_ID"].ToString();
                ForumTitle_Label.Text = Session["ForumTitle"].ToString();
                ForumCreated_Label.Text = Session["ForumCreated"].ToString();
                ForumTitle = Session["ForumTitle"].ToString();
                DataBind();

                loadContent();

            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {


        }




        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString);
            try
            {
                con.Open();
                string query1 = "insert into ForumContent (ForumTitle, Content, CreatedBy) values (@ForumTitle, @Content, @CreatedBy) ";
                SqlCommand cmd1 = new SqlCommand(query1, con);
                cmd1.Parameters.AddWithValue("@ForumTitle", (Session["ForumTitle"].ToString()));
                cmd1.Parameters.AddWithValue("@Content", TextBox4.Text);
                cmd1.Parameters.AddWithValue("@CreatedBy", (Session["User_ID"].ToString()));
                cmd1.ExecuteNonQuery();
                Response.Redirect("CustomerForumContent.aspx");

                con.Close();
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.ToString());
            }
        }


        void loadContent()
        {
            string cs = ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [ForumContent] WHERE ForumTitle ='" + Session["ForumTitle"].ToString() + "'";
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            GridView1.DataSource = dt;

            GridView1.DataBind();
            con.Close();
        }
    }

}
