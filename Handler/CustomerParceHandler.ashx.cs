﻿using Liliveri.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;

namespace Liliveri.Handler
{
    /// <summary>
    /// CustomerParceHandler 的摘要说明
    /// </summary>
    public class CustomerParceHandler : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            var result = string.Empty;
            string requestMethod = context.Request["requestMethod"].ToString();
            context.Response.ContentType = "text/plain";
            switch (requestMethod)
            {
                //获取包裹列表数据
                case "GetCustomerParceList":
                    result = GetCustomerParceList($"{context.Session["User_ID"]}", context.Request["keyWord"]);
                    break;
                //删除包裹数据
                case "RemoveCustomerParce":
                    result = RemoveCustomerParce(context.Request["id"].ToString());
                    break;
                //添加包裹数据
                case "AddCustomerParce":
                    result = AddCustomerParce(context.Request["TrackingNumber"].ToString(), context.Request["ItemName"].ToString(), context.Request["Departure"].ToString(), context.Request["Arrival"].ToString(), context.Request["Weight"].ToString(), context.Request["Time"].ToString(), context.Request["Recorder"].ToString(), $"{context.Session["User_ID"]}");
                    break;
                default:
                    break;
            }
            context.Response.Write(result);
        }

        /// <summary>
        /// 获取包裹列表数据
        /// </summary>
        /// <param name="UserId">当前登录用户</param>
        /// <param name="keyWord">快递单号</param>
        /// <returns></returns>
        private string GetCustomerParceList(string UserId, object keyWord)
        {
            List<Entity_Parce> parceList = new List<Entity_Parce>();
            if (!string.IsNullOrEmpty(UserId))
            {
                //1、创建数据库连接对象，并编写连接字符串，注意连接字符串不要写错
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString);
                con.Open();
                var sqlCondition = string.Empty;
                if (keyWord != null)
                {
                    sqlCondition = $"AND TrackingNumber like '%{keyWord}%'";
                }
                //2、编写操作语句 TSQL语句
                string sql = $"select * from Parcel where CreatorID ='{UserId}' AND DelFlag=0 {sqlCondition}";
                //3、创建数据库操作对象，创建过程是与刚创建的连接对象匹配起来
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Entity_Parce parce = new Entity_Parce();
                        parce.Id = int.Parse(dr[0].ToString());
                        parce.TrackingNumber = dr[1].ToString();
                        parce.ItemName = dr[2].ToString();
                        parce.Departure = dr[3].ToString();
                        parce.Arrival = dr[4].ToString();
                        parce.Weight = int.Parse(dr[5].ToString());
                        parce.Time = dr[6].ToString();
                        parce.Recorder = dr[7].ToString();
                        parce.CreatorID = dr[8].ToString();
                        parce.WriteTime = DateTime.Parse(dr[9].ToString());
                        parceList.Add(parce);
                    }
                }
                con.Close();
            }
            return new JavaScriptSerializer().Serialize(parceList);
        }

        /// <summary>
        /// 删除包裹数据
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        private string RemoveCustomerParce(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                //1、创建数据库连接对象，并编写连接字符串，注意连接字符串不要写错
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString);

                //2、创建数据库操作对象，创建过程是与刚创建的连接对象匹配起来
                SqlCommand cmd = conn.CreateCommand();

                //3、编写操作语句 TSQL语句
                cmd.CommandText = $"update Parcel set DelFlag=1 where id={id}";

                //4、数据库连接打开，准备执行操作
                conn.Open();

                //5、执行操作，并记录受影响的行数
                int count = cmd.ExecuteNonQuery();

                //6、关闭数据库连接**********
                conn.Close();

                //7、提示操作是否成功
                if (count > 0)
                    return "Deleted Successfully";
                else
                    return "Failed to delete";
            }
            return "Empty ID";
        }

        /// <summary>
        /// 添加包裹数据
        /// </summary>
        /// <param name="TrackingNumber">快递单号</param>
        /// <param name="ItemName"></param>
        /// <param name="Departure"></param>
        /// <param name="Arrival"></param>
        /// <param name="Weight"></param>
        /// <param name="Time"></param>
        /// <param name="Recorder"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        private string AddCustomerParce(string TrackingNumber, string ItemName, string Departure, string Arrival, string Weight, string Time, string Recorder, string UserId)
        {
            //1、创建数据库连接对象，并编写连接字符串，注意连接字符串不要写错
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString);
            try
            {
                //2、数据库连接打开，准备执行操作
                con.Open();
                string query1 = $"select count(*) from Parcel where TrackingNumber ='{TrackingNumber}'";

                //3、创建数据库操作对象，创建过程是与刚创建的连接对象匹配起来
                SqlCommand cmd = new SqlCommand(query1, con);
                int check = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if (check > 0)
                {
                    return "Tracking Number already exist.";
                }
                else
                {
                    //3、编写操作语句 TSQL语句
                    string query2 = "insert into Parcel(TrackingNumber, ItemName, Departure, Arrival, Weight, Time, Recorder,CreatorID,WriteTime,DelFlag) values (@TrackingNumber, @ItemName, @Departure, @Arrival, @Weight, @Time, @Recorder,@CreatorID,@WriteTime,@DelFlag) ";
                    SqlCommand cmd1 = new SqlCommand(query2, con);
                    cmd1.Parameters.AddWithValue("@TrackingNumber", TrackingNumber);
                    cmd1.Parameters.AddWithValue("@ItemName", ItemName);
                    cmd1.Parameters.AddWithValue("@Departure", Departure);
                    cmd1.Parameters.AddWithValue("@Arrival", Arrival);
                    cmd1.Parameters.AddWithValue("@Weight", Weight);
                    cmd1.Parameters.AddWithValue("@Time", Time);
                    cmd1.Parameters.AddWithValue("@Recorder", Recorder);
                    cmd1.Parameters.AddWithValue("@CreatorID", UserId);
                    cmd1.Parameters.AddWithValue("@WriteTime", DateTime.Now);
                    cmd1.Parameters.AddWithValue("@DelFlag", 0);
                    //4、执行操作，并记录受影响的行数
                    cmd1.ExecuteNonQuery();
                    return "Record Successful!";
                }
            }
            catch (Exception ex)
            {
                return "ERROR:" + ex.ToString();
            }
            finally
            {
                //5、关闭数据库连接**********
                con.Close();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}