﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Liliveri.About" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">




<!-- SITE TITLE -->
<title>Liliveri</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="shortcut icon" href="img/favicon.ico">

<!-- =========================
     STYLESHEETS   
============================== -->

<!-- STYLES FILE -->   
<link href="css/master.css" rel="stylesheet">	
        
<!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

	<!-- =========================
	 PRE LOADER
	============================== -->

	<div class="preloader" id="preloader">
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
	</div>

	<!-- =========================
	 END PRE LOADER
	============================== -->
	<!-- =========================
		TOP MAIN NAVBAR
	============================== -->
	<div class="main-navbar main-navbar-1" id="main-navbar">
		<div class="container">
			<div class="row">

				<!-- === TOP LOGO === -->

				<div class="logo" id="main-logo">
					<div class="logo-image">
						<img src="img/logo.png" alt="" />
					</div>
					<div class="logo-text">
						Liliveri
					</div>
				</div>

				<!-- === TOP SEARCH === -->

				<div class="main-search-input" id="main-search-input">
					<form>
						<input type="text" id="main-search" name="main-search" placeholder="Try and type enter..." />
					</form>
				</div>



				<div class="show-menu-control">
					<!-- === top search button show === -->
					<a id="show-menu" class="show-menu" href="#">
						<div class="my-btn my-btn-primary">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-bars"></i>
							</div>
						</div>
					</a>
				</div>

				<!-- === TOP MENU === -->

				<div class="collapse navbar-collapse main-menu main-menu-1" id="main-menu">
					<ul class="nav navbar-nav">

						<!-- === top menu item === -->
						<li class="active dropdown">
							<a href="Default.aspx">Home</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a href="Service_Detail.aspx">Service</a>

						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li>
							<a href="About.html">About us</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">Delivering Helper</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="Fare_Calculator.aspx">Fare Calculator</a>
								</li>
								<li>
									<a href="Tracking.html">Tracking No.</a>
								</li>

							</ul>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li>
							<a class="latest" href="Contacts.aspx">Contacts</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">Client Area</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="Login.aspx">Login</a>
								</li>
								<li>
									<a href="Register.aspx">Create Free Account</a>
								</li>
							</ul>
						</li>

					</ul>
				</div>

			</div>
		</div>
	</div>

	<!-- =========================
		END TOP MAIN NAVBAR
	============================== -->
	<!-- ===================================
		PAGE HEADER
	======================================== -->
	<div class="page-header" data-stellar-background-ratio="0.4">
		<div class="page-header-overlay"></div>
		<div class="container">
			<div class="row">

				<!-- === PAGE HEADER TITLE === -->
				<div class="page-header-title">
					<h2>ABOUT US</h2>
				</div>

				<!-- === PAGE HEADER BREADCRUMB === -->
				<div class="page-header-breadcrumb">
					<ol class="breadcrumb">
						<li><a href="Default.aspx">Home</a></li>
						<li class="active">About Us</li>
					</ol>
				</div>


			</div>
		</div>
	</div>
	<!-- ===================================
		END PAGE HEADER
	======================================== -->
	<!-- =========================
		ABOUT TEXT
	============================== -->
	<div class="def-section about-text">
		<div class="container">
			<div class="about-text-image">
				<img src="media/about/1.jpg" alt="" />
			</div>
			<h2>We Offer Best Shopping Experience</h2>

			<style>
				p {
					font-family: sans-serif; /*若电脑不支持宋体，则用仿宋，若不支持仿宋，则在sans-serif中找*/
					font-weight: bold;
					font-size: 100%;
					
				}
			</style>
			<p>
				Liliveri's vision is to achieve customer success, promote the economy, and develop national express.  <br />
				The core values of Liliveri are to respect every employee,to provide employees with sufficient and fair opportunities for development,<br />
				to innovate and to lead continuously.<br />
				Achievement customers and partners a win-win team collaboration,<br />
				and the overall consistent maintain a high level of social responsibility,<br />
				do a respected corporate citizen is the soul of enterprise employees, <br />
				enterprise and staff grow up together is the foundation to realize the vision,<br />
				respect for each employee, and provide employees development space and an equal chance, <br />
				to keep the motive force of the enterprise development, to innovation and development,<br />
				To maintain the advantage of continuous leading, <br />
				and to the greatest extent to meet customer needs, customer success.<br />
			</p>
		</div>
	</div>
	<!-- =========================
		END ABOUT TEXT
	============================== -->
	<!-- =========================
		ABOUT TEAM
	============================== -->
	<div class="def-section about-team">
		<div class="container">
			<div class="row">

				<!-- === TEAM ITEM === -->

				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<div class="team-item">
						<div class="team-item-image">
							<div class="team-item-image-overlay">
								<div class="team-item-icons">
									<a href="http://www.apu.edu.my/"><i class="fa fa-facebook"></i></a>
								</div>
							</div>
							<img src="media/team/1.jpg" alt="" />
						</div>
						<div class="team-item-name">
							Chen Xihao
						</div>
						<div class="team-item-position">
							DIRECTOR
						</div>
					</div>
				</div>

				<!-- === TEAM ITEM === -->

				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<div class="team-item">
						<div class="team-item-image">
							<div class="team-item-image-overlay">
								<div class="team-item-icons">
									<a href="http://www.apu.edu.my/"><i class="fa fa-facebook"></i></a>
								</div>
							</div>
							<img src="media/team/2.jpg" alt="" />
						</div>
						<div class="team-item-name">
							Lai Yong Wei
						</div>
						<div class="team-item-position">
							SHOP DIRECTOR
						</div>
					</div>
				</div>

				<!-- === TEAM ITEM === -->

				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<div class="team-item">
						<div class="team-item-image">
							<div class="team-item-image-overlay">
								<div class="team-item-icons">
									<a href="http://www.apu.edu.my/"><i class="fa fa-facebook"></i></a>
								</div>
							</div>
							<img src="media/team/4.jpg" alt="" />
						</div>
						<div class="team-item-name">
							Wang Zikai
						</div>
						<div class="team-item-position">
							SHOP DIRECTOR
						</div>
					</div>
				</div>

				<!-- === TEAM ITEM === -->

				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<div class="team-item">
						<div class="team-item-image">
							<div class="team-item-image-overlay">
								<div class="team-item-icons">
									<a href="http://www.apu.edu.my/"><i class="fa fa-facebook"></i></a>
								</div>
							</div>
							<img src="media/team/6.jpg" alt="" />
						</div>
						<div class="team-item-name">
							Luo Wenhao
						</div>
						<div class="team-item-position">
							Developer
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- =========================
		END ABOUT TEAM
	============================== -->
	<!-- ===================================
		SECTION REVIEWS AND FAQ
	======================================== -->
	<section class="def-section">
		<div class="container">
			<div class="row">

				<!-- === REVIEWS === -->
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="home-review">

						<!-- === TITLE GROUP === -->
						<div class="title-group">
							<h2>COMMENT</h2>
							<div class="subtitle with-square">The voice of the customer</div>
						</div>

						<!-- === OWL CAROUSEL === -->
						<div class="home-review-carousel owl-carousel owl-theme" id="owl-review">

							<!-- === OWL CAROUSEL ITEM === -->
							<div class="home-review-carousel-item">
								<div class="home-review-carousel-text">
									<div class="home-review-carousel-quote"><div><i class="fa fa-quote-left"></i></div></div>
									<p>
										Liliveri is a very reliable company. I am very satisfied with their logistics, 
										express delivery and delivery men, whether it is long-distance moving or online shopping
									</p>
								</div>

								<!-- === OWL CAROUSEL ITEM AUTHOR=== -->
								<div class="home-review-carousel-people">
									<div class="home-review-carousel-avatar">
										<img src="media/avatars/avatar2.png" alt="" />
									</div>
									<div class="home-review-carousel-name">
										Beathag Stamatios
									</div>
									<div class="home-review-carousel-company">
										Used Liliveri more than 10 times
									</div>
								</div>
							</div>

							<!-- === OWL CAROUSEL ITEM === -->
							<div class="home-review-carousel-item">
								<div class="home-review-carousel-text">
									<div class="home-review-carousel-quote"><div><i class="fa fa-quote-left"></i></div></div>
									<p>
										I was shocked by Liliveri's speed.The package, which originally took 3 to 5 days,
										only took 2 days to reach its destination after passing through Liliveri, which is incredible
									</p>
								</div>

								<!-- === OWL CAROUSEL ITEM AUTHOR=== -->
								<div class="home-review-carousel-people">
									<div class="home-review-carousel-avatar">
										<img src="media/avatars/avatar1.png" alt="" />
									</div>
									<div class="home-review-carousel-name">
										Evgenios Xenia
									</div>
									<div class="home-review-carousel-company">
										Used Liliveri more than 5 times
									</div>
								</div>
							</div>

							<!-- === OWL CAROUSEL ITEM === -->
							<div class="home-review-carousel-item">
								<div class="home-review-carousel-text">
									<div class="home-review-carousel-quote"><div><i class="fa fa-quote-left"></i></div></div>
									<p>
										The service attitude of Liliveri is very good. 
										When the goods inside were found damaged in my hands, 
										they immediately verified and paid the relevant amount.That's something to trust.
									</p>
								</div>

								<!-- === OWL CAROUSEL ITEM AUTHOR=== -->
								<div class="home-review-carousel-people">
									<div class="home-review-carousel-avatar">
										<img src="media/avatars/avatar3.png" alt="" />
									</div>
									<div class="home-review-carousel-name">
										Lefteris Areti
									</div>	
									<div class="home-review-carousel-company">
										Used Liliveri more than 100 times
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- === FAQ === -->
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="home-faq">

						<!-- === TITLE GROUP === -->
						<div class="title-group">
							<h2>WHY CHOSE US</h2>
							<div class="subtitle with-square">PELENTESQUE INYD URNA</div>
						</div>

						<!-- === ACCORDION === -->
						<div class="panel-group" id="accordion">

							<!-- === ACCORDION ITEM === -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
											Reasonably priced
										</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in">
									<div class="panel-body">
										<p>
											Liliveri uses a reasonable pricing scheme that allows you to always
											deliver your packages at the most reasonable price.
										</p>
									</div>
								</div>
							</div>

							<!-- === ACCORDION ITEM === -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
											Thoughtful after-sales service
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse">
									<div class="panel-body">
										<p>
											We have 7*24 hours uninterrupted customer service, convenient and direct communication and contact with customers.
											Any questions can be found through our email or online message and other ways to find us.
										</p>
									</div>
								</div>
							</div>

							<!-- === ACCORDION ITEM === -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
											A logistics service covering the whole of Malaysia 
										</a>
									</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse">
									<div class="panel-body">
										<p>
											Liliveri's services cover the whole of Malaysia, 
											no matter where you are in Malaysia.Liliveri can safely deliver your package to your side.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- ===================================
		END SECTION REVIEWS AND FAQ
	======================================== -->
	<!-- ===================================
		SUBSCRIBE SECTION
	======================================== -->
	<div class="def-section home-subscribe">
		<div class="container">
			<div class="row">

				<!-- === SUBSCRIBE TEXT === -->
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 home-subscribe-text animated wow fadeInUp" data-wow-duration=".5s" data-wow-offset="100">
					<span class="home-subscribe-icon"><i class="flaticon-email114"></i></span>
					SIGN UP FOR NEWSLETTER TO GET UPDATES AND NEWS
				</div>

				<!-- === SUBSCRIBE FORM === -->
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 home-subscribe-form animated wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s" data-wow-offset="100">
					<form>
						<div class="home-subscribe-form-input">
							<input type="text" name="subscribe" placeholder="YOUR E-MAIL" />
						</div>
						<div class="home-subscribe-form-button">
							<button>
								<span class="my-btn my-btn-primary">
									<span class="my-btn-bg-top"></span>
									<span class="my-btn-bg-bottom"></span>
									<span class="my-btn-text">
										SUBSCRIBE
									</span>
								</span>
							</button>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
	<!-- ===================================
		END SUBSCRIBE SECTION
	======================================== -->
	<!-- ===================================
		FOOTER
	======================================== -->
	<footer class="def-section footer">
		<div class="container">
			<div class="row">

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
					<div class="logo with-border-bottom">
						<div class="logo-image">
							<img src="img/logo.png" alt="" />
						</div>
						<div class="logo-text">
							Liliveri
						</div>
					</div>
					<div class="footer-1-text">
						<p>We recommended Google Chrome and Microsoft Edge. </p>
					</div>
					<div class="footer-1-button">
						<a href="About.html">
							<div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									MORE
								</div>
							</div>
						</a>
					</div>
				</div>

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
					<h4 class="with-square with-border-bottom">LINKS</h4>
					<div class="footer-2-links">
						<div class="footer-2-links-1">
							<ul>
								<li><a href="Default.aspx">Home</a></li>
								<li><a href="Service_Detail.aspx">Service</a></li>
								<li><a href="About.html">About us</a></li>
								<li><a href="Login.aspx">Clients</a></li>
							</ul>
						</div>
						<div class="footer-2-links-2">
							<ul>
								<li><a href="Contacts.aspx">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
					<h4 class="with-square with-border-bottom">ADRESS</h4>
					<div class="footer-3-phone footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-phone"></i></span>
						Telephone:  + 390 12 345 6789
					</div>
					<div class="footer-3-fax footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-fax"></i></span>
						Fax/phone:  + 390 12 345 6789
					</div>
					<div class="footer-3-mail footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
						E-mail:  customerrelationship@liliveri.com.my
					</div>
					<div class="footer-3-adress footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-map-marker"></i></span>
						Adress: Welman Street, Rawang Town, Rawang 48000, Selangor Dahrul Ehsan
					</div>
				</div>

			</div>
		</div>
	</footer>
	<!-- ===================================
		END FOOTER
	======================================== -->
	<!-- ===================================
		BOTTOM SECTION
	======================================== -->
	<div class="bottom">
		<div class="container">
			<div class="row">

				<!-- === BOTTOM LEFT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
					COPYRIGHT 2021 | Liliveri
				</div>

				<!-- === BOTTOM CENTER === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-twitter"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-facebook"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-google-plus"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-pinterest-p"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-instagram"></i>
							</div>
						</div>
					</a>
				</div>

				<!-- === BOTTOM RIGHT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">
					<a href="#">TERM OF USE</a> |
					MADE BY <a href="#">Liliveri</a>
				</div>

			</div>
		</div>
	</div>
	<!-- ===================================
		END BOTTOM SECTION
	======================================== -->
	<!-- =========================
	   SLIDE MENU
	============================== -->
	<aside id="slide-menu" class="slide-menu">

		<!-- === CLOSE MENU BUTON === -->
		<div class="close-menu" id="close-menu">
			<i class="fa fa-close"></i>
		</div>

		<!-- === SLIDE MENU === -->
		<ul id="left-menu" class="left-menu">

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="Default.aspx">Home <i class="fa fa-plus arrow"></i></a>

			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Service <i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="Service_Detail.aspx">Service Details</a></li>
				</ul>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="About.html">About us</a>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Delivering Helper <i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="07_typography.html">Fare Calculator</a></li>
					<li><a href="Tracking.html">Tracking No.</a></li>
				</ul>
			</li>
			<li>
				<a href="Contacts.aspx">Contacts</a>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Client Area<i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="Register.aspx">Create a free account</a></li>
					<li><a href="Login.aspx">Login</a></li>
				</ul>
			</li>

			<!-- === SLIDE MENU ITEM === -->

		</ul>

	</aside>
	<!-- =========================
	   END SLIDE MENU
	============================== -->

	<!-- =========================
	   BLACK OVERLAY
	============================== -->
	<div class="black-overlay" id="black-overlay"></div>
	<!-- =========================
	   END BLACK OVERLAY
	============================== -->
	<!-- =========================
		 SCRIPTS
	============================== -->
	<!-- JQUERY -->
	<script src="js/jquery-1.11.3.min.js"></script>

	<!-- BOOTSTRAP -->
	<script src="js/bootstrap.min.js"></script>

	<!-- SMOOTH SCROLLING  -->
	<script src="js/smoothscroll.min.js"></script>

	<!-- STELLAR.JS FOR PARALLAX -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- SLIDER PRO  -->
	<script src="assets/slider-pro/js/jquery.sliderPro.min.js"></script>

	<!-- SCROLLSPY -->
	<script src="js/scrollspy.min.js"></script>

	<!-- WOW PLAGIN -->
	<script src="js/wow.min.js"></script>

	<!-- CAROUSEL -->
	<script src="assets/owl-carousel/owl.carousel.min.js"></script>

	<!-- VERTICAL ACCORDEON MENU -->
	<script src="js/metisMenu.min.js"></script>

	<!-- CUSTOM SCRIPT -->
	<script src="js/theme.min.js"></script>

</body>
</html>