﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fare_Calculator.aspx.cs" Inherits="Liliveri.Fare_Calculator" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- SITE TITLE -->
    <title>Liliveri- Sending Parcel from home</title>

    <!-- =========================
          FAV AND TOUCH ICONS
    ============================== -->
    <link rel="shortcut icon" href="img/favicon.ico">

    <!-- =========================
         STYLESHEETS
    ============================== -->
    <!-- STYLES FILE -->
    <link href="css/master.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        function myFunction() {
            var from = document.getElementsByName("from");
            var to = document.getElementsByName("to");
            var Weight = document.getElementsByName("Weight");


            if (from[0].selectedIndex == 0) {
                alert("Please Choose Departure Address");
            }
            else if (to[0].selectedIndex == 0) {
                alert("Please Choose Arrivals  Address");
            }
            else if (Weight[0].value == "") {
                alert("Please Fill Pacel Weight");
            }
            else (from[0].selectedValue == "Kuala Lumpur" ||
                to[0].selectedValue == "Kuala Lumpur" ||
                Weight[0].value == "1")
            {
                alert("RM 10 ");
            }


        }

    </script>

</head>

<body>

    <!-- =========================
     PRE LOADER
    ============================== -->

    <div class="preloader" id="preloader">
        <div class="cssload-container">
            <div class="cssload-whirlpool"></div>
        </div>
    </div>

    <!-- =========================
     END PRE LOADER
    ============================== -->
    <!-- =========================
        TOP MAIN NAVBAR
    ============================== -->
    <div class="main-navbar main-navbar-1" id="main-navbar">
        <div class="container">
            <div class="row">

                <!-- === TOP LOGO === -->

                <div class="logo" id="main-logo">
                    <div class="logo-image">
                        <img src="img/logo.png" alt="" />
                    </div>
                    <div class="logo-text">
                        Liliveri
                    </div>
                </div>

                <!-- === TOP SEARCH === -->

                <div class="main-search-input" id="main-search-input">
                    <form>
                        <input type="text" id="main-search" name="main-search" placeholder="Try and type enter..." />
                    </form>
                </div>



                <div class="show-menu-control">
                    <!-- === top search button show === -->
                    <a id="show-menu" class="show-menu" href="#">
                        <div class="my-btn my-btn-primary">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- === TOP MENU === -->

                <div class="collapse navbar-collapse main-menu main-menu-1" id="main-menu">
                    <ul class="nav navbar-nav">

                        <!-- === top menu item === -->
                        <li class="active dropdown">
                            <a href="Default.aspx">Home</a>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li class="dropdown">
                            <a href="Service_Detail.aspx">Service</a>

                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li>
                            <a href="About.aspx">About us</a>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#">Delivering Helper</a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="Fare_Calculator.aspx">Fare Calculator</a>
                                </li>
                                <li>
                                    <a href="Tracking.aspx">Tracking No.</a>
                                </li>

                            </ul>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li>
                            <a class="latest" href="Contacts.aspx">Contacts</a>
                        </li>
                        <li class="main-menu-separator"></li>
                        <!-- === top menu item === -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="#">Client Area</a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="Login.aspx">Login</a>
                                </li>
                                <li>
                                    <a href="Register.aspx">Create Free Account</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!-- =========================
        END TOP MAIN NAVBAR
    ============================== -->
    <!-- ===================================
        PAGE HEADER
    ======================================== -->
    <div class="page-header" data-stellar-background-ratio="0.4">
        <div class="page-header-overlay"></div>
        <div class="container">
            <div class="row">

                <!-- === PAGE HEADER TITLE === -->
                <div class="page-header-title">
                    <h2>Fare Calculator</h2>
                </div>

                <!-- === PAGE HEADER BREADCRUMB === -->
                <div class="page-header-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Fare Calculator</li>
                    </ol>
                </div>


            </div>
        </div>
    </div>
    <!-- ===================================
        END PAGE HEADER
    ======================================== -->
    <!-- =========================
        ELEMENTS
    ============================== -->
    <div class="def-section elem-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

                    <!-- === LISTS === -->

                    <h3 class="bs-example-title">User Instructions</h3>

                    <div class="bs-example">
                        <ul>
                            <li>Select from location</li>
                            <li>Then Choose the destination</li>

                        </ul>
                    </div>

                </div>

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                    <!-- === HEADINGS === -->

                    <h3 class="bs-example-title">Fare Calculator</h3>


                    <div class="bs-example">

                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="first-top">
                                        <h1>Enter the following information:</h1>
                                        <h3>
                                            Departure Address
                                        </h3>
                                        <select class="bs-example" name="from" id="from_select">
                                            <option value=""> </option>
                                            <option value="Kuala Lumpur">Kuala Lumpur</option>
                                            <option value="Labuan">Labuan</option>
                                            <option value="Putrajaya">Putrajaya</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Malacca">Malacca</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Pahang">Pahang</option>
                                            <option value="Penang">Penang</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Sabah">Sabah</option>
                                            <option value="Sarawak">Sarawak</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Terengganu">Terengganu</option>

                                        </select>
                                        <h3>
                                            Arrivals Address
                                        </h3>
                                        <select class="bs-example" name="to" id="to_select">
                                            <option value=""> </option>
                                            <option value="Kuala Lumpur">Kuala Lumpur</option>
                                            <option value="Labuan">Labuan</option>
                                            <option value="Putrajaya">Putrajaya</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Malacca">Malacca</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Pahang">Pahang</option>
                                            <option value="Penang">Penang</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Sabah">Sabah</option>
                                            <option value="Sarawak">Sarawak</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Terengganu">Terengganu</option>
                                        </select>
                                        <h3>
                                            Weight
                                        </h3>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 contacts-form-item">
                                            <input type="text" name="Weight" id="Weight" placeholder="Unit:KG" oninput="value=value.match(/-?[0-9]*\.?[0-9]*/)">
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contacts-form-item contacts-form-button">
                                             <button id="Calculator" onclick="myFunction()">
                                                <span class="my-btn my-btn-grey">
                                                    <span class="my-btn-bg-top"></span>
                                                    <span class="my-btn-bg-bottom"></span>
                                                    <span class="my-btn-text">
                                                        Calculator
                                                    </span>
                                                </span>
                                            </button>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- ===================================
        END ELEMENTS
    ======================================== -->
    <!-- ===================================
        SUBSCRIBE SECTION
    ======================================== -->
    <div class="def-section home-subscribe">
        <div class="container">
            <div class="row">

                <!-- === SUBSCRIBE TEXT === -->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 home-subscribe-text animated wow fadeInUp" data-wow-duration=".5s" data-wow-offset="100">
                    <span class="home-subscribe-icon"><i class="flaticon-email114"></i></span>
                    SIGN UP FOR NEWSLETTER TO GET UPDATES AND NEWS
                </div>

                <!-- === SUBSCRIBE FORM === -->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 home-subscribe-form animated wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s" data-wow-offset="100">
                    <form>
                        <div class="home-subscribe-form-input">
                            <input type="text" name="subscribe" placeholder="YOUR E-MAIL" />
                        </div>
                        <div class="home-subscribe-form-button">
                            <button>
                                <span class="my-btn my-btn-primary">
                                    <span class="my-btn-bg-top"></span>
                                    <span class="my-btn-bg-bottom"></span>
                                    <span class="my-btn-text">
                                        SUBSCRIBE
                                    </span>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- ===================================
        END SUBSCRIBE SECTION
    ======================================== -->
    <!-- ===================================
        FOOTER
    ======================================== -->
    <footer class="def-section footer">
        <div class="container">
            <div class="row">

                <!-- === FOOTER COLUMN === -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
                    <div class="logo with-border-bottom">
                        <div class="logo-image">
                            <img src="img/logo.png" alt="" />
                        </div>
                        <div class="logo-text">
                            Liliveri
                        </div>
                    </div>
                    <div class="footer-1-text">
                        <p>We recommended Google Chrome and Microsoft Edge. </p>
                    </div>
                    <div class="footer-1-button">
                        <a href="About.aspx">
                            <div class="my-btn my-btn-primary">
                                <div class="my-btn-bg-top"></div>
                                <div class="my-btn-bg-bottom"></div>
                                <div class="my-btn-text">
                                    MORE
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <!-- === FOOTER COLUMN === -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
                    <h4 class="with-square with-border-bottom">LINKS</h4>
                    <div class="footer-2-links">
                        <div class="footer-2-links-1">
                            <ul>
                                <li><a href="Default.aspx">Home</a></li>
                                <li><a href="Service_Detail.aspx">Service</a></li>
                                <li><a href="About.aspx">About us</a></li>
                                <li><a href="Login.aspx">Clients</a></li>
                            </ul>
                        </div>
                        <div class="footer-2-links-2">
                            <ul>
                                <li><a href="Contacts.aspx">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- === FOOTER COLUMN === -->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
                    <h4 class="with-square with-border-bottom">ADRESS</h4>
                    <div class="footer-3-phone footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-phone"></i></span>
                        Telephone:  + 390 12 345 6789
                    </div>
                    <div class="footer-3-fax footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-fax"></i></span>
                        Fax/phone:  + 390 12 345 6789
                    </div>
                    <div class="footer-3-mail footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
                        E-mail:  customerrelationship@liliveri.com.my
                    </div>
                    <div class="footer-3-adress footer-3-item">
                        <span class="footer-3-icon"><i class="fa fa-map-marker"></i></span>
                        Adress: Welman Street, Rawang Town, Rawang 48000, Selangor Dahrul Ehsan
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <!-- ===================================
        END FOOTER
    ======================================== -->
    <!-- ===================================
        BOTTOM SECTION
    ======================================== -->
    <div class="bottom">
        <div class="container">
            <div class="row">

                <!-- === BOTTOM LEFT === -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
                    COPYRIGHT 2021 | Liliveri
                </div>

                <!-- === BOTTOM CENTER === -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-twitter"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-google-plus"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-pinterest-p"></i>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="my-btn my-btn-grey">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-instagram"></i>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- === BOTTOM RIGHT === -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">
                    <a href="#">TERM OF USE</a> |
                    MADE BY <a href="#">Liliveri</a>
                </div>

            </div>
        </div>
    </div>
    <!-- ===================================
        END BOTTOM SECTION
    ======================================== -->
    <!-- =========================
       SLIDE MENU
    ============================== -->
    <aside id="slide-menu" class="slide-menu">

        <!-- === CLOSE MENU BUTON === -->
        <div class="close-menu" id="close-menu">
            <i class="fa fa-close"></i>
        </div>

        <!-- === SLIDE MENU === -->
        <ul id="left-menu" class="left-menu">

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="Default.aspx">Home <i class="fa fa-plus arrow"></i></a>

            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="#">Service <i class="fa fa-plus arrow"></i></a>

                <!-- === slide menu child === -->
                <ul class="slide-menu-child">
                    <li><a href="Service_Detail.aspx">Service Details</a></li>
                </ul>
            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="About.aspx">About us</a>
            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="#">Delivering Helper <i class="fa fa-plus arrow"></i></a>

                <!-- === slide menu child === -->
                <ul class="slide-menu-child">
                    <li><a href="Fare_Calculator.aspx">Fare Calculator</a></li>
                    <li><a href="Tracking.aspx">Tracking No.</a></li>
                </ul>
            </li>
            <li>
                <a href="Contacts.aspx">Contacts</a>
            </li>

            <!-- === SLIDE MENU ITEM === -->
            <li>
                <a href="#">Client Area<i class="fa fa-plus arrow"></i></a>

                <!-- === slide menu child === -->
                <ul class="slide-menu-child">
                    <li><a href="Register.aspx">Create a free account</a></li>
                    <li><a href="Login.aspx">Login</a></li>
                </ul>
            </li>

            <!-- === SLIDE MENU ITEM === -->

        </ul>

    </aside>
    <!-- =========================
       END SLIDE MENU
    ============================== -->
    <!-- =========================
       BLACK OVERLAY
    ============================== -->
    <div class="black-overlay" id="black-overlay"></div>
    <!-- =========================
       END BLACK OVERLAY
    ============================== -->
    <!-- =========================
         SCRIPTS
    ============================== -->
    <!-- JQUERY -->
    <script src="js/jquery-1.11.3.min.js"></script>

    <!-- BOOTSTRAP -->
    <script src="js/bootstrap.min.js"></script>

    <!-- SMOOTH SCROLLING  -->
    <script src="js/smoothscroll.min.js"></script>

    <!-- STELLAR.JS FOR PARALLAX -->
    <script src="js/jquery.stellar.min.js"></script>

    <!-- SLIDER PRO  -->
    <script src="assets/slider-pro/js/jquery.sliderPro.min.js"></script>

    <!-- SCROLLSPY -->
    <script src="js/scrollspy.min.js"></script>

    <!-- WOW PLAGIN -->
    <script src="js/wow.min.js"></script>

    <!-- CAROUSEL -->
    <script src="assets/owl-carousel/owl.carousel.min.js"></script>

    <!-- VERTICAL ACCORDEON MENU -->
    <script src="js/metisMenu.min.js"></script>

    <!-- CUSTOM SCRIPT -->
    <script src="js/theme.min.js"></script>


</body>
</html>
