﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Liliveri.ResetPassword" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- SITE TITLE -->
<title>Liliveri</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="shortcut icon" href="img/favicon.ico">

<!-- =========================
     STYLESHEETS   
============================== -->

<!-- STYLES FILE -->   
<link href="css/master.css" rel="stylesheet">	
        
<!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

	<!-- =========================
	 PRE LOADER
	============================== -->

	<div class="preloader" id="preloader">
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
	</div>

	<!-- =========================
	 END PRE LOADER
	============================== -->
	<!-- =========================
		TOP MAIN NAVBAR
	============================== -->
	<div class="main-navbar main-navbar-1" id="main-navbar">
		<div class="container">
			<div class="row">

				<!-- === TOP LOGO === -->

				<div class="logo" id="main-logo">
					<div class="logo-image">
						<img src="img/logo.png" alt="" />
					</div>
					<div class="logo-text">
						Liliveri
					</div>
				</div>

				<!-- === TOP SEARCH === -->

				<div class="main-search-input" id="main-search-input">
					<form>
						<input type="text" id="main-search" name="main-search" placeholder="Try and type enter..." />
					</form>
				</div>



				<div class="show-menu-control">
					<!-- === top search button show === -->
					<a id="show-menu" class="show-menu" href="#">
						<div class="my-btn my-btn-primary">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-bars"></i>
							</div>
						</div>
					</a>
				</div>

				<!-- === TOP MENU === -->

				<div class="collapse navbar-collapse main-menu main-menu-1" id="main-menu">
					<ul class="nav navbar-nav">

						<!-- === top menu item === -->
						<li class="active dropdown">
							<a href="Default.aspx">Home</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a href="Service_Detail.aspx">Service</a>

						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li>
							<a href="About.aspx">About us</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">Delivering Helper</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="Fare_Calculator.aspx">Fare Calculator</a>
								</li>
								<li>
									<a href="Tracking.aspx">Tracking No.</a>
								</li>

							</ul>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li>
							<a class="latest" href="Contacts.aspx">Contacts</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">Client Area</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="Login.aspx">Login</a>
								</li>
								<li>
									<a href="Register.aspx">Create Free Account</a>
								</li>
							</ul>
						</li>

					</ul>
				</div>

			</div>
		</div>
	</div>

	<!-- =========================
		END TOP MAIN NAVBAR
	============================== -->
	<!-- ===================================
		PAGE HEADER
	======================================== -->
	<div class="page-header" data-stellar-background-ratio="0.4">
		<div class="page-header-overlay"></div>
		<div class="container">
			<div class="row">

				<!-- === PAGE HEADER TITLE === -->
				<div class="page-header-title">
					<h2>Forgot Password</h2>
				</div>

				<!-- === PAGE HEADER BREADCRUMB === -->
				<div class="page-header-breadcrumb">
					<ol class="breadcrumb">
						<li><a href="Default.aspx">Home</a></li>
						<li class="active">Forgot Password</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
	<!-- ===================================
		END PAGE HEADER
	======================================== -->
	<!---- Forgot Password Part ---->
       <form id="form1" runat="server">  
            <div>  
                <table style="width:100%;">  
                    <caption class="style6">  
                        <strong>ForgetPassword:</strong>  
                    </caption>  
                    <tr>  
                        <td class="style1">  
   
                        </td>  
                        <td class="style2">  
   
                        </td>  
                        <td>  
 </td>  
                        <td>  
 </td>  
                    </tr>  
                    <tr>  
                        <td class="style1">  
   
                        </td>  
                        <td class="style2">  
EmailId:  
                        </td>  
                        <td>  
                            <asp:TextBox ID="TextBox1" runat="server">  
                            </asp:TextBox>  
                        </td>  
                        <td>  
 </td>  
                    </tr>  
                    <tr>  
                        <td class="style1">  
   
                        </td>  
                        <td class="style2">  
   
                        </td>  
                        <td>  
 </td>  
                        <td>  
 </td>  
                    </tr>  
                    <tr>  
                        <td class="style1">  
                            <asp:Label ID="lbmsg" runat="server">  
                            </asp:Label>  
                        </td>  
                        <td class="style2">  
                            <asp:LinkButton CssClass="btn btn-primary" ID="LinkButton1" runat="server" onclick="LinkButton1_Click">Back to Login  
                            </asp:LinkButton>  
                        </td>  
                        <td>  
                            <asp:Button CssClass="btn btn-primary" ID="Button1" runat="server" onclick="Button1_Click" Text="Submit"/>  
                        </td>  
                        <td>  
 </td>  
                    </tr>  
                </table>  
            </div>  
        </form>  
		


	<!-- ===================================
		FOOTER
	======================================== -->
	<footer class="def-section footer">
		<div class="container">
			<div class="row">

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
					<div class="logo with-border-bottom">
						<div class="logo-image">
							<img src="img/logo.png" alt="" />
						</div>
						<div class="logo-text">
							Liliveri
						</div>
					</div>
					<div class="footer-1-text">
						<p>We recommended Google Chrome and Microsoft Edge. </p>
					</div>
					<div class="footer-1-button">
						<a href="About.aspx">
							<div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									MORE
								</div>
							</div>
						</a>
					</div>
				</div>

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
					<h4 class="with-square with-border-bottom">LINKS</h4>
					<div class="footer-2-links">
						<div class="footer-2-links-1">
							<ul>
								<li><a href="Default.aspx">Home</a></li>
								<li><a href="Service_Detail.aspx">Service</a></li>
								<li><a href="About.aspx">About us</a></li>
								<li><a href="Login.aspx">Clients</a></li>
							</ul>
						</div>
						<div class="footer-2-links-2">
							<ul>
								<li><a href="Contacts.aspx">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
					<h4 class="with-square with-border-bottom">ADRESS</h4>
					<div class="footer-3-phone footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-phone"></i></span>
						Telephone:  + 390 12 345 6789
					</div>
					<div class="footer-3-fax footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-fax"></i></span>
						Fax/phone:  + 390 12 345 6789
					</div>
					<div class="footer-3-mail footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
						E-mail:  customerrelationship@liliveri.com.my
					</div>
					<div class="footer-3-adress footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-map-marker"></i></span>
						Adress: Welman Street, Rawang Town, Rawang 48000, Selangor Dahrul Ehsan
					</div>
				</div>

			</div>
		</div>
	</footer>
	<!-- ===================================
		END FOOTER
	======================================== -->
	<!-- ===================================
		BOTTOM SECTION
	======================================== -->
	<div class="bottom">
		<div class="container">
			<div class="row">

				<!-- === BOTTOM LEFT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
					COPYRIGHT 2021 | Liliveri
				</div>

				<!-- === BOTTOM CENTER === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-twitter"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-facebook"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-google-plus"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-pinterest-p"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-instagram"></i>
							</div>
						</div>
					</a>
				</div>

				<!-- === BOTTOM RIGHT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">
					<a href="#">TERM OF USE</a> |
					MADE BY <a href="#">Liliveri</a>
				</div>

			</div>
		</div>
	</div>
	<!-- ===================================
		END BOTTOM SECTION
	======================================== -->
	<!-- =========================
	   SLIDE MENU
	============================== -->
	<aside id="slide-menu" class="slide-menu">

		<!-- === CLOSE MENU BUTON === -->
		<div class="close-menu" id="close-menu">
			<i class="fa fa-close"></i>
		</div>

		<!-- === SLIDE MENU === -->
		<ul id="left-menu" class="left-menu">

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="Default.aspx">Home <i class="fa fa-plus arrow"></i></a>

			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Service <i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="Service_Detail.aspx">Service Details</a></li>
				</ul>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="About.aspx">About us</a>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Delivering Helper <i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="07_typography.html">Fare Calculator</a></li>
					<li><a href="Tracking.aspx">Tracking No.</a></li>
				</ul>
			</li>
			<li>
				<a href="Contacts.aspx">Contacts</a>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Client Area<i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="Register.aspx">Create a free account</a></li>
					<li><a href="Login.aspx">Login</a></li>
				</ul>
			</li>

			<!-- === SLIDE MENU ITEM === -->

		</ul>

	</aside>
	<!-- =========================
	   END SLIDE MENU
	============================== -->
	<!-- =========================
	   BLACK OVERLAY
	============================== -->
	<div class="black-overlay" id="black-overlay"></div>
	<!-- =========================
	   END BLACK OVERLAY
	============================== -->
	<!-- =========================
		 SCRIPTS
	============================== -->
	<!-- JQUERY -->
	<script src="js/jquery-1.11.3.min.js"></script>

	<!-- BOOTSTRAP -->
	<script src="js/bootstrap.min.js"></script>

	<!-- SMOOTH SCROLLING  -->
	<script src="js/smoothscroll.min.js"></script>

	<!-- STELLAR.JS FOR PARALLAX -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- SLIDER PRO  -->
	<script src="assets/slider-pro/js/jquery.sliderPro.min.js"></script>

	<!-- SCROLLSPY -->
	<script src="js/scrollspy.min.js"></script>

	<!-- WOW PLAGIN -->
	<script src="js/wow.min.js"></script>

	<!-- CAROUSEL -->
	<script src="assets/owl-carousel/owl.carousel.min.js"></script>

	<!-- VERTICAL ACCORDEON MENU -->
	<script src="js/metisMenu.min.js"></script>

	<!-- CUSTOM SCRIPT -->
	<script src="js/theme.min.js"></script>

</body>
</html>