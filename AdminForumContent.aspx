﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminForumContent.aspx.cs" Inherits="Liliveri.AdminForumContent" %>




<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">




<!-- SITE TITLE -->
<title>Liliveri</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="shortcut icon" href="img/favicon.ico">

<!-- =========================
     STYLESHEETS   
============================== -->

<!-- STYLES FILE -->   
<link href="css/master.css" rel="stylesheet">	
     
				<style>
	 .center{
		 margin:auto;
		 width:60%;
		 border:3px solid #0094ff;
		 padding:10px;
	 }
	</style>   
<!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
	<form id="form1" runat="server">

	<!-- =========================
	 PRE LOADER
	============================== -->

	<div class="preloader" id="preloader">
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
	</div>

	<!-- =========================
	 END PRE LOADER
	============================== -->
	<!-- =========================
		TOP MAIN NAVBAR
	============================== -->
	<div class="main-navbar main-navbar-1" id="main-navbar">
		<div class="container">
			<div class="row">
                 
				<!-- === TOP LOGO === -->
				 
				<div class="logo" id="main-logo">
					<div class="logo-image">
						<img src="img/logo.png" alt="" />
					</div>
					<div class="logo-text">
						Liliveri
					</div>
				</div>
				 

				 

				
				<div class="show-menu-control">
					<!-- === top search button show === -->
					<a id="show-menu" class="show-menu" href="#">
						<div class="my-btn my-btn-primary">
                            <div class="my-btn-bg-top"></div>
                            <div class="my-btn-bg-bottom"></div>
                            <div class="my-btn-text">
                                <i class="fa fa-bars"></i>
                            </div>
						</div>
					</a>
				</div>
				 
				<!-- === TOP MENU === -->
								 
				<div class="collapse navbar-collapse main-menu main-menu-1" id="main-menu">
					<ul class="nav navbar-nav">
											
						<!-- === top menu item === -->
						<li class="active dropdown">
							<a href="AdminHome.aspx">Home</a>
						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a  href="AdminManageParcel.aspx">Manage Parcel</a>

						</li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li>
							<a href="AdminForum.aspx">Forum</a>
						</li> 
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a  href="AdminManageUser.aspx">Manage User</a>

						<li class="main-menu-separator"></li>
						<li class="main-menu-separator"></li>
						<!-- === top menu item === -->
						<li class="dropdown">
							<a data-toggle="dropdown" href="#"><asp:Label ID="username_label" runat="server" Text="username_label"></asp:Label> Information</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="CustomerChangeDetails.aspx">Update Details</a>
								</li>
								<li>
									<a href="Logout.aspx">Logout</a>
								</li>
							</ul>
						</li> 
	
					</ul>
				</div>

			</div>
		</div>
	</div>

	<!-- =========================
		END TOP MAIN NAVBAR
	============================== -->
	<!-- ===================================
		PAGE HEADER
	======================================== -->
	<div class="page-header" data-stellar-background-ratio="0.4">
		<div class="page-header-overlay"></div>
		<div class="container">
			<div class="row">

				<!-- === PAGE HEADER TITLE === -->
				<div class="page-header-title">
					<h2>Admin Forum Content</h2>
				</div>

				<!-- === PAGE HEADER BREADCRUMB === -->
				<div class="page-header-breadcrumb">
					<ol class="breadcrumb">
						<li><a href="AdminHome.aspx">Home</a></li>
						<li class="active">Admin Forum Content</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
	<!-- ===================================
		END PAGE HEADER
	======================================== -->
		<!---- Forum Title Part ---->
	  <div class="container">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
            <div id="banner">
                <h1>
                   <asp:Label ID="ForumTitle_Label" runat="server" Text="ForumTitle_Label" name="ForumTitle"></asp:Label></h1>
      
				<br />
				<h3> Created By： <asp:Label ID="ForumCreated_Label" runat="server" Text="ForumCreated_Label"></asp:Label></h3>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="registrationform">
                <div class="form-horizontal">
                    <fieldset>
   
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text="Forum Content" CssClass="col-lg-2 control-label"></asp:Label>
                            <div class="col-lg-10">
                                <asp:TextBox ID="TextBox4" runat="server" placeholder="Forum Content" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                      

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Add a new comment" OnClick="btnSubmit_Click1" OnClientClick="javascript:alert('You have submitted your comment successfully.');"/>
                                                      
                            </div>
	
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
	                  <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Liliveri %>" SelectCommand="SELECT * FROM [ForumContent] WHERE ForumTitle ='<%# ForumTitle%>'"></asp:SqlDataSource>
	                  <div class="row">
                     <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Liliveri %>" SelectCommand="SELECT * FROM [ForumContent] WHERE ForumTitle ='<%# ForumTitle%>'" FilterExpression="ForumTitle ='<%# ForumTitle%>' "></asp:SqlDataSource>
                     <div class="col">
                        <asp:GridView class="center" ID="GridView1" runat="server" DataKeyNames="ForumContentID" OnRowCreated="GridView1_RowCreated" >
                          
							<Columns>
                           </Columns>
                        </asp:GridView>
                     </div>
                  </div>

	<!-- ===================================
		FOOTER
	======================================== -->
	<footer class="def-section footer">
		<div class="container">
			<div class="row">

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
					<div class="logo with-border-bottom">
						<div class="logo-image">
							<img src="img/logo.png" alt="" />
						</div>
						<div class="logo-text">
							Liliveri
						</div>
					</div>
					<div class="footer-1-text">
						<p>We recommended Google Chrome and Microsoft Edge. </p>
					</div>
					<div class="footer-1-button">
						<a href="About.aspx">
							<div class="my-btn my-btn-primary">
								<div class="my-btn-bg-top"></div>
								<div class="my-btn-bg-bottom"></div>
								<div class="my-btn-text">
									MORE
								</div>
							</div>
						</a>
					</div>
				</div>

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
					<h4 class="with-square with-border-bottom">LINKS</h4>
					<div class="footer-2-links">
						<div class="footer-2-links-1">
							<ul>
								<li><a href="Default.aspx">Home</a></li>
								<li><a href="Service_Detail.aspx">Service</a></li>
								<li><a href="About.aspx">About us</a></li>
								<li><a href="Login.aspx">Clients</a></li>
							</ul>
						</div>
						<div class="footer-2-links-2">
							<ul>
								<li><a href="Contacts.aspx">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- === FOOTER COLUMN === -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
					<h4 class="with-square with-border-bottom">ADRESS</h4>
					<div class="footer-3-phone footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-phone"></i></span>
						Telephone:  + 390 12 345 6789
					</div>
					<div class="footer-3-fax footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-fax"></i></span>
						Fax/phone:  + 390 12 345 6789
					</div>
					<div class="footer-3-mail footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
						E-mail:  customerrelationship@liliveri.com.my
					</div>
					<div class="footer-3-adress footer-3-item">
						<span class="footer-3-icon"><i class="fa fa-map-marker"></i></span>
						Adress: Welman Street, Rawang Town, Rawang 48000, Selangor Dahrul Ehsan
					</div>
				</div>

			</div>
		</div>
	</footer>
	<!-- ===================================
		END FOOTER
	======================================== -->
	<!-- ===================================
		BOTTOM SECTION
	======================================== -->
	<div class="bottom">
		<div class="container">
			<div class="row">

				<!-- === BOTTOM LEFT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
					COPYRIGHT 2021 | Liliveri
				</div>

				<!-- === BOTTOM CENTER === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-twitter"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-facebook"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-google-plus"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-pinterest-p"></i>
							</div>
						</div>
					</a>
					<a href="#">
						<div class="my-btn my-btn-grey">
							<div class="my-btn-bg-top"></div>
							<div class="my-btn-bg-bottom"></div>
							<div class="my-btn-text">
								<i class="fa fa-instagram"></i>
							</div>
						</div>
					</a>
				</div>

				<!-- === BOTTOM RIGHT === -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">
					<a href="#">TERM OF USE</a> |
					MADE BY <a href="#">Liliveri</a>
				</div>

			</div>
		</div>
	</div>
	<!-- ===================================
		END BOTTOM SECTION
	======================================== -->
	<!-- =========================
	   SLIDE MENU
	============================== -->
	<aside id="slide-menu" class="slide-menu">

		<!-- === CLOSE MENU BUTON === -->
		<div class="close-menu" id="close-menu">
			<i class="fa fa-close"></i>
		</div>

		<!-- === SLIDE MENU === -->
		<ul id="left-menu" class="left-menu">

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="Default.aspx">Home <i class="fa fa-plus arrow"></i></a>

			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Service <i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="Service_Detail.aspx">Service Details</a></li>
				</ul>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="About.aspx">About us</a>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Delivering Helper <i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="07_typography.html">Fare Calculator</a></li>
					<li><a href="Tracking.aspx">Tracking No.</a></li>
				</ul>
			</li>
			<li>
				<a href="Contacts.aspx">Contacts</a>
			</li>

			<!-- === SLIDE MENU ITEM === -->
			<li>
				<a href="#">Client Area<i class="fa fa-plus arrow"></i></a>

				<!-- === slide menu child === -->
				<ul class="slide-menu-child">
					<li><a href="Register.aspx">Create a free account</a></li>
					<li><a href="Login.aspx">Login</a></li>
				</ul>
			</li>

			<!-- === SLIDE MENU ITEM === -->

		</ul>

	</aside>
	<!-- =========================
	   END SLIDE MENU
	============================== -->
	<!-- =========================
	   BLACK OVERLAY
	============================== -->
	<div class="black-overlay" id="black-overlay"></div>
	<!-- =========================
	   END BLACK OVERLAY
	============================== -->
	<!-- =========================
		 SCRIPTS
	============================== -->
	<!-- JQUERY -->
	<script src="js/jquery-1.11.3.min.js"></script>

	<!-- BOOTSTRAP -->
	<script src="js/bootstrap.min.js"></script>

	<!-- SMOOTH SCROLLING  -->
	<script src="js/smoothscroll.min.js"></script>

	<!-- STELLAR.JS FOR PARALLAX -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- SLIDER PRO  -->
	<script src="assets/slider-pro/js/jquery.sliderPro.min.js"></script>

	<!-- SCROLLSPY -->
	<script src="js/scrollspy.min.js"></script>

	<!-- WOW PLAGIN -->
	<script src="js/wow.min.js"></script>

	<!-- CAROUSEL -->
	<script src="assets/owl-carousel/owl.carousel.min.js"></script>

	<!-- VERTICAL ACCORDEON MENU -->
	<script src="js/metisMenu.min.js"></script>

	<!-- CUSTOM SCRIPT -->
	<script src="js/theme.min.js"></script>
</form>
</body>
</html>
