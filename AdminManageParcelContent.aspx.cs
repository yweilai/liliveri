﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace Liliveri
{
    public partial class AdminManageParcelContent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_ID"] != null)
                {
                    username_label.Text = Session["User_ID"].ToString();
                    CartTrackingNo.Text = Session["ParcelTrackingNo"].ToString();
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }

        protected void btn_SubmitUpdate(object sender, EventArgs e)
        {
            string cs = ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            con.Open();

            string query2 = "insert into ParcelStatus(TrackingNumber, Status, CreatedBy, CreatedTime) values (@TrackingNumber, @Status, @CreatedBy, @CreatedTime) ";
            SqlCommand cmd2 = new SqlCommand(query2, con);
            cmd2.Parameters.AddWithValue("@TrackingNumber", Convert.ToInt32(Session["ParcelTrackingNo"].ToString()));
            cmd2.Parameters.AddWithValue("@CreatedBy", Session["User_ID"].ToString());
            cmd2.Parameters.AddWithValue("@CreatedTime", DateTime.Now);
            cmd2.Parameters.AddWithValue("@Status", DropDownList1.SelectedValue);
           
            cmd2.ExecuteNonQuery();


            lbmsg.Text = "Update has successfully done! You will be directed in a few seconds";
            string url = "AdminManageParcel.aspx";
            StringBuilder js = new StringBuilder("<script language=\"javascript\">")
                    .Append("var ts = 3; setInterval(\"redirect()\",1000);")
                    .Append("function redirect(){  if(ts == 0){")
                    .Append("window.location.href=\"" + url + "\"; }else{")
                    .Append("document.body.innerHTML = \"msg \" + (ts--)+\"seconds\";}}")
                    .Append("</script>");
            Response.Write(js.ToString());

        }
    }
    

}