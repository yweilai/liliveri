﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Liliveri
{
    public partial class CustomerCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_ID"] != null)
                {
                    username_label.Text = Session["User_ID"].ToString();
                    DataBind();
                    loadContent();
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            
        }

        void loadContent()
        {
            string cs = ConfigurationManager.ConnectionStrings["Liliveri"].ConnectionString;
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [Parcel] WHERE CreatorID ='" + Session["User_ID"].ToString() + "'";
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            
            da.Fill(dt);
            DataColumn newCol = new DataColumn("RowNumber", typeof(string));
            dt.Columns.Add(newCol);
            int totalrow = 0;
            foreach (DataRow row in dt.Rows)
            {
                totalrow++;
                row["RowNumber"] = totalrow.ToString();
            }
            dt2 = dt;
            GridView1.DataSource = dt2;
            GridView1.DataBind();
            con.Close();
        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {


        }


        protected void GridView1_DataBound(object sender, EventArgs e)
        {

        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "CartPay")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[rowIndex-1];
                string CartId = row.Cells[2].Text;
                string CartTrackingNo = row.Cells[3].Text;
                string ItemName = row.Cells[4].Text;
                string CartDeparture = row.Cells[5].Text;
                string CartArrival = row.Cells[6].Text;
                string CartWeight = row.Cells[7].Text;
                string CartTime = row.Cells[8].Text;
                string CartRecorder = row.Cells[9].Text;
                string CartCreatorId = row.Cells[10].Text;
                string CartWriteTime = row.Cells[11].Text;
                string DelFlag = row.Cells[12].Text;
                Session["CartId"] = CartId;
                Session["CartTrackingNo"] = CartTrackingNo;
                Session["CartItemName"] = ItemName;
                Session["CartDeparture"] = CartDeparture;
                Session["CartArrival"] = CartArrival;
                Session["CartWeight"] = CartWeight;
                Session["CartTime"] = CartTime;
                Session["CartRecorder"] = CartRecorder;
                Session["CartCretorId"] = CartCreatorId;
                Session["CartWriteTme"] = CartWriteTime;
                Session["DelFlag"] = DelFlag;
                Response.Redirect("CustomerCartPay.aspx");

            }
        }

    }
}
